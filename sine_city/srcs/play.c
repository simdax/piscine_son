/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   play.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 09:32:13 by scornaz           #+#    #+#             */
/*   Updated: 2018/10/26 09:49:11 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "sine.h"

void	init_tables(t_app *app)
{
	int		i;
	double	phase;

	i = 0;
	while (i < TABLE_SIZE)
	{
		phase = (double)i / TABLE_SIZE;
		app->sine[i] = sin(phase * M_PI * 2);
		app->sine_plus[i] =
			(sin(phase * M_PI * 2) * 0.5) + (sin(phase * M_PI * 4) * 0.5) +
			(sin(phase * M_PI * 12) * 0.3) + (sin(phase * M_PI * 7) * 0.2) +
			(sin(phase * M_PI * 6) * 0.08) + (sin(phase * M_PI * 9) * 0.02);
		app->triangle[i] = phase < 0.5 ? (phase * 4 - 1) : (phase * -4 + 3);
		app->saw[i] = (phase * 2) + (phase < 0.5 ? 0 : -2);
		app->square[i] = phase < 0.5 ? 1 : -1;
		app->perc[i] = phase < 0.5 ? (phase * 2) : (phase * -2 + 2);
		++i;
	}
}

float	interpolate(double *sine, double index)
{
	float	value1;
	float	value2;
	float	frac;

	frac = index - (int)index;
	value1 = sine[(int)index];
	value2 = sine[(int)index + 1];
	return (value1 + frac * (value2 - value1));
}

void	note_fill_buffer(t_voice *voice, t_note *note, double ddur, double pos)
{
	double	phase;
	double	dur;
	double	index;
	int		i;

	i = BUFFER_SIZE * pos;
	dur = BUFFER_SIZE * ddur;
	phase = note->frequency / BASE_FREQ;
	index = voice->ptr_lut;
	while (i < dur)
	{
		voice->buffer[i] += interpolate(voice->lut, index) * note->volume;
		//* interpolate(voice->env,
		//(1 - note->cycles / note->total_cycles) * TABLE_SIZE);
		index += phase;
		if (index >= TABLE_SIZE)
			index -= TABLE_SIZE;
		++i;
	}
	voice->ptr_lut = index;
}

void	voice_fill_buffer(void *el, void *data, t_array *a)
{
	t_voice *voice;
	t_note	*note;
	double	cycle;
	double	cycle_curr_note;

	voice = el;
	if (voice_check_finish(voice))
		return ;
	cycle = 1;
	while (cycle && voice->notes)
	{
		note = voice->notes->content;
		cycle_curr_note = note->cycles;
		if (cycle_curr_note > cycle)
			cycle_curr_note = cycle;
		note_fill_buffer(voice, note, cycle_curr_note, 1 - cycle);
		cycle -= cycle_curr_note;
		note->cycles -= cycle_curr_note;
		if (!note->cycles)
		{
			voice->notes = voice->notes->next;
			voice_check_finish(voice);
		}
	}
}

int		play_buffer(PaStream *stream, float *buffer)
{
	float	stereo_out[BUFFER_SIZE][2];
	float	max;
	float	min;
	int		i;

	max = 0;
	min = 0;
	i = 0;
	while (i < BUFFER_SIZE)
	{
		if (buffer[i] > max)
			max = buffer[i];
		else if (buffer[i] < min)
			min = buffer[i];
		++i;
	}
	i = -1;
	while (++i < BUFFER_SIZE)
	{
		stereo_out[i][1] = buffer[i];
		stereo_out[i][0] = stereo_out[i][1];
		/*
		 ** printf("min max %f %f\n", min, max);
		 ** stereo_out[i][0] = stereo_out[i][1] =
		 ** (((buffer[i] - min) / (max - min)) - 0.5 );
		 ** printf("%f\n", (stereo_out[i][0]));
		*/
	}
	return (Pa_WriteStream(stream, stereo_out, BUFFER_SIZE));
}
