/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 09:12:15 by scornaz           #+#    #+#             */
/*   Updated: 2018/10/26 09:45:00 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdlib.h>
#include "portaudio.h"
#include "pa_linux_alsa.h"
#include "libft.h"
#include "printf/includes/printf.h"
#include "sine.h"

void			play_notes(t_app *app)
{
	int			finished;

	finished = 1;
	array_for_each2(app->voices, &finished, must_continue);
	while (!finished)
	{
		array_for_each2(app->voices, app->buffer, voice_fill_buffer);
		g_err = play_buffer(app->stream, app->buffer);
		if (g_err != paNoError)
			handle_error();
		ft_bzero(app->buffer, sizeof(float) * BUFFER_SIZE);
		finished = 1;
		array_for_each2(app->voices, &finished, must_continue);
	}
}

int				main(int argc, char **argv)
{
	t_app		app;

	if (argc == 1)
		return (usage());
	ft_printf("SINE CITY\nplaying : %s\n", argv[1]);
	app.voices = array_new(sizeof(t_voice), 8);
	while (*(++argv))
	{
		if (!parse_midi(*argv, &app))
			return (1);
	}
	ft_printf("nb of tracks : %d\n", app.voices->cursor);
	init_tables(&app);
	init_stream(&app.stream);
	play_notes(&app);
	stop_stream(app.stream);
	return (0);
}
