/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   callbacks.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 09:03:09 by scornaz           #+#    #+#             */
/*   Updated: 2018/10/26 09:33:50 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sine.h"
#include "printf/includes/printf.h"

int				usage(void)
{
	ft_printf("must provide a music file\n");
	return (1);
}

void			must_continue(void *voice, void *boolean, t_array *a)
{
	*(int*)boolean = *(int*)boolean && ((t_voice*)voice)->finished;
}

static void		repeat(t_list *el)
{
	t_note	*note;

	note = el->content;
	note->cycles = note->total_cycles;
}

int				voice_check_finish(t_voice *voice)
{
	if (!voice->notes)
	{
		if (!voice->repeat)
		{
			voice->finished = 1;
			return (1);
		}
		else
		{
			voice->notes = voice->first_note;
			ft_lstiter(voice->notes, repeat);
		}
	}
	return (0);
}
