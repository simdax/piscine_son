/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 09:17:50 by scornaz           #+#    #+#             */
/*   Updated: 2018/10/26 09:49:47 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "portaudio.h"
#include "libft.h"
#include "sine.h"

void	handle_error(void)
{
	const	PaHostErrorInfo *host_error_info = Pa_GetLastHostErrorInfo();

	fprintf(stderr, "An error occured while using the portaudio stream\n");
	fprintf(stderr, "Error number: %d\n", g_err);
	fprintf(stderr, "Error message: %s\n", Pa_GetErrorText(g_err));
	if (g_err == paUnanticipatedHostError)
	{
		fprintf(stderr, "Host API error = #%ld, hostApiType = %d\n",
				host_error_info->errorCode, host_error_info->hostApiType);
		fprintf(stderr, "Host API error = %s\n", host_error_info->errorText);
	}
	Pa_Terminate();
	exit(g_err);
}

void	stop_stream(PaStream *stream)
{
	g_err = Pa_StopStream(stream);
	if (g_err != paNoError)
		handle_error();
	g_err = Pa_CloseStream(stream);
	if (g_err != paNoError)
		handle_error();
	Pa_Terminate();
}

void	init_stream(PaStream **stream)
{
	PaStreamParameters	output_parameters;

	g_err = Pa_Initialize();
	if (g_err != paNoError)
		handle_error();
	output_parameters.device = Pa_GetDefaultOutputDevice();
	if (output_parameters.device == paNoDevice)
		handle_error();
	output_parameters.channelCount = 2;
	output_parameters.sampleFormat = paFloat32;
	output_parameters.suggestedLatency =
		Pa_GetDeviceInfo(output_parameters.device)->defaultLowOutputLatency;
	output_parameters.hostApiSpecificStreamInfo = NULL;
	g_err = Pa_OpenStream(
			stream, NULL, &output_parameters,
			SAMPLE_RATE, BUFFER_SIZE,
			paClipOff, NULL, NULL);
	if (g_err != paNoError)
		handle_error();
	g_err = Pa_StartStream(*stream);
	if (g_err != paNoError)
		handle_error();
}

double	note_to_freq(double degree, double octave)
{
	return (pow(2, 5 + octave) * pow(pow(2, (double)1 / 12), degree));
}

double	midi_to_freq(double note)
{
	return (note_to_freq(fmod(note, 12), (int)note / 12));
}
