/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/26 09:34:25 by scornaz           #+#    #+#             */
/*   Updated: 2018/10/26 09:44:19 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "portaudio.h"
#include <stdlib.h>
#include "libft.h"
#include "printf/includes/printf.h"
#include "sine.h"

void			new_voice(t_app *app, char **split_line)
{
	t_voice voice_tmp;

	array_add(app->voices, &voice_tmp, 1);
	CURRENT_VOICE->finished = 0;
	CURRENT_VOICE->notes = 0;
	CURRENT_VOICE->ptr_lut = 0;
	CURRENT_VOICE->env = app->perc;
	CURRENT_VOICE->buffer = app->buffer;
	CURRENT_VOICE->repeat = split_line[2] && split_line[2][0] == 'R';
	if (ft_strequ("sine", split_line[1]))
		CURRENT_VOICE->lut = app->sine_plus;
	else if (ft_strequ("triangle", split_line[1]))
		CURRENT_VOICE->lut = app->triangle;
	else if (ft_strequ("square", split_line[1]))
		CURRENT_VOICE->lut = app->square;
	else if (ft_strequ("saw", split_line[1]))
		CURRENT_VOICE->lut = app->saw;
	else
		exit(1);
}

void			new_note(t_app *app, char **split_line,
		t_coefficients *coefficients)
{
	t_note	note_tmp;
	int		first;

	first = CURRENT_VOICE->repeat && !CURRENT_VOICE->notes;
	note_tmp.frequency = midi_to_freq(atof(split_line[0])
			+ coefficients->tonalite);
	note_tmp.dur = atof(split_line[1]) * coefficients->tempo;
	note_tmp.cycles = note_tmp.dur * ((double)SAMPLE_RATE / BUFFER_SIZE);
	note_tmp.total_cycles = note_tmp.cycles;
	note_tmp.volume = atof(split_line[2]) * coefficients->volume;
	ft_lstaddlast(&CURRENT_VOICE->notes,
			ft_lstnew(&note_tmp, sizeof(t_note)));
	if (first)
		CURRENT_VOICE->first_note = CURRENT_VOICE->notes;
}

void			new_coefficients(char **split_line,
		t_coefficients *coefficients)
{
	coefficients->tonalite = atof(split_line[1]);
	coefficients->tempo = atof(split_line[2]);
	coefficients->volume = atof(split_line[3]);
}

int				parse_midi(char *path, t_app *app)
{
	char			*line;
	char			**split_line;
	int				fd;
	t_coefficients	coefficients;

	coefficients = (t_coefficients){0, 1, 1};
	fd = open(path, O_RDONLY);
	while (fd && get_next_line(fd, &line))
	{
		split_line = ft_strsplit(line, ' ');
		if (split_line[0][0] == '#')
			new_voice(app, split_line);
		else if (split_line[0][0] == '>')
			new_coefficients(split_line, &coefficients);
		else
			new_note(app, split_line, &coefficients);
		ft_free_strsplit(split_line);
		free(line);
	}
	return (1);
}
