#ifndef SINE_H
# define SINE_H

# define SAMPLE_RATE  48000
# define BUFFER_SIZE  512
# define TABLE_SIZE   4096 
# define GAIN		  0.5
# define DELTA		  ((double)SAMPLE_RATE / BUFFER_SIZE) 
# define BASE_FREQ	  ((double)SAMPLE_RATE / TABLE_SIZE)  
# define CURRENT_VOICE ((t_voice*)array_at(app->voices->cursor - 1, app->voices))

# include <portaudio.h>
# include "libft.h"

static PaError g_err;

typedef struct	s_note{
	double  frequency;
	double  volume;
	double  cycles;
	double  total_cycles;
	double  dur;
}				t_note;

typedef struct	s_coefficients
{
	int	  tonalite;  
	float   tempo;  
	float   volume;  
}				t_coefficients;

typedef struct	s_voice
{
	t_list		  *notes;
	int			  repeat;
	t_list		  *first_note;
	int			  finished;
	PaStream		  *stream;
	pthread_t		  thread;
	double		  ptr_lut;
	double		  *lut;
	double		  *env;
	float			  *buffer;
}				t_voice;

typedef struct	s_app{
	PaStream	 *stream;
	t_array	 *voices;
	float		 buffer[BUFFER_SIZE];
	double	 sine[TABLE_SIZE];
	double	 sine_plus[TABLE_SIZE];
	double	 saw[TABLE_SIZE];
	double	 triangle[TABLE_SIZE];
	double	 square[TABLE_SIZE];
	double	 perc[TABLE_SIZE];
}				t_app;

int		parse_midi(char *path, t_app *app);
void	init_tables(t_app *app);
void	voice_fill_buffer(void *el, void *data, t_array *a);
int		play_buffer(PaStream *stream, float *buffer);
void	must_continue(void *voice, void *boolean, t_array *a);
double	midi_to_freq(double note);
int		voice_check_finish(t_voice *voice);
int		usage();
void	handle_error(void);
void	init_stream(PaStream **stream);
void	stop_stream(PaStream *stream);

#endif
