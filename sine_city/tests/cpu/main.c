#include <portaudio.h>
#include <math.h>
#include <stdio.h>

#define BUF_SIZE   4096 

int main()
{
    PaStream                *stream;
    PaStreamParameters      output;
    float                   buffer[BUF_SIZE];
    float                   max;
    float                   min;
    float                   sum;

    for (int i = 0; i < BUF_SIZE; i++)
    {
        sum = (float)sin(((double)i * 2 * M_PI * 30 * 1) / BUF_SIZE) * 1
        + (float)sin(((double)i * 2 * M_PI * 30 * 15.2) / BUF_SIZE) * 0.14
        + (float)sin(((double)i * 2 * M_PI * 30 * 10.1) / BUF_SIZE) * 0.3;
        max = max < sum ? sum : max; 
        min = min > sum ? sum : min; 
        buffer[i] = sum;
    }
    for (int i = 0; i < BUF_SIZE; i++)
        buffer[i] = (((buffer[i] - min) / (max - min)) * 2 - 1) * 0.25;
    Pa_Initialize();
    output = (PaStreamParameters){.device=Pa_GetDefaultOutputDevice(), .channelCount = 1, .sampleFormat = paFloat32, .suggestedLatency=0.5, .hostApiSpecificStreamInfo = 0};
    Pa_OpenStream(&stream, 0, &output, 44100, BUF_SIZE, 0, 0, 0);
    Pa_StartStream(stream);
    for (int i = 0; i < 3 * 44100 / BUF_SIZE ; ++i)
        Pa_WriteStream(stream, buffer, BUF_SIZE); 
    Pa_StopStream(stream);
    Pa_CloseStream(stream);
    printf("termine on plie bagage!");
    Pa_Terminate();
}
