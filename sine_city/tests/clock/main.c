#include <time.h>
#include <stdio.h>
#include <pthread.h>
#include "heap.h"
#define NTHREADS 24

void *thread_function(void *);
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
int   counter = 0;
#define BUF_SIZE  100000
int   buffer[BUF_SIZE];

int main(void)
{
  int j, i;
  clock_t t;
  int numbers[10] = {3, 1, 5, 3, 5, 3, 4, 6, 2, 5};
  t_array  *array = array_new(sizeof(t_heap), 16);
  
  for (i=0; i < 10; i++)
    heap_insert(array, numbers[i], 0);
  
  pthread_t thread_id[NTHREADS];
  for(i=0; i < NTHREADS; i++)
  {
    pthread_create( &thread_id[i], NULL, thread_function, NULL );
  }
  for(j=0; j < NTHREADS; j++)
  {
    pthread_join( thread_id[j], NULL);
  } 
  i=BUF_SIZE;
  while(i--)
    printf("%d", buffer[i]);
}

void *thread_function(void *dummyPtr)
{
  int i=BUF_SIZE;
     
  pthread_mutex_lock( &mutex1 );
  while (i--)
  {
    if (buffer[i])
      buffer[i] = 0; 
    else
      buffer[i] = 1;
  }
  pthread_mutex_unlock( &mutex1 );
}
