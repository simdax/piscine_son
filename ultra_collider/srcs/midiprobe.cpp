// midiprobe.cpp
//
// Simple program to check MIDI inputs and outputs.
//
// by Gary Scavone, 2003-2012.

#include <iostream>
#include <cstdlib>
#include <map>
#include <rtmidi/RtMidi.h>

int print_info()
{
    // Create an api map.
    std::map<int, std::string> apiMap;
    apiMap[RtMidi::MACOSX_CORE] = "OS-X CoreMIDI";
    apiMap[RtMidi::WINDOWS_MM] = "Windows MultiMedia";
    apiMap[RtMidi::UNIX_JACK] = "Jack Client";
    apiMap[RtMidi::LINUX_ALSA] = "Linux ALSA";
    apiMap[RtMidi::RTMIDI_DUMMY] = "RtMidi Dummy";
    std::vector< RtMidi::Api > apis;
    RtMidi :: getCompiledApi( apis );

    std::cout << "\nCompiled APIs:\n";
    for ( unsigned int i=0; i<apis.size(); i++ )
        std::cout << "  " << apiMap[ apis[i] ] << std::endl;

    RtMidiOut midiout;
    try {
        unsigned int nPorts;
        std::cout << "\nCurrent output API: " << apiMap[ midiout.getCurrentApi() ] << std::endl;
        nPorts = midiout.getPortCount();
        std::cout << "\nThere are " << nPorts << " MIDI output ports available.\n";
        for ( unsigned i=0; i<nPorts; i++ ) {
            std::string portName = midiout.getPortName(i);
            std::cout << "  Output Port #" << i+1 << ": " << portName << std::endl;
        }
        std::cout << std::endl;
    } catch ( RtMidiError &error ) {
        error.printMessage();
    }
    return 0;
}
