#include <unistd.h>
#include <iostream>
#include <sys/wait.h>
#include <unistd.h>
#include <stdarg.h>

void    watcher(pid_t *pids)
{
  int     status;
  pid_t   pid;

  printf("WELCOME !\n");
  pid = wait(&status);
  printf("BYE :*\n");
  while (*pids)
    kill(*pids++, SIGTERM);
}

pid_t   launch_process(char *path, ...)
{
  pid_t         pid;
  char          *args[15];
  char          **args_ptr;
  va_list       va_args;
  int           i;

  i = 0;
  args_ptr = &args[0] ;
  va_start(va_args, path);
  args_ptr[i++] = path;
  while((args_ptr[i] = va_arg(va_args, char*)))
    i++; 
  args_ptr[i] = 0;
  va_end(va_args);
  pid = fork();
  if (pid == -1)
    perror("fork problem");
  else if (pid == 0)
    execv(path, args); 
  if (pid > 0)
    return (pid);
  return (0);
}

#define DEFAULT_SYNTHS 3
#define MAX_SYNTHS 4

int usage()
{
  printf("max synth : %d", MAX_SYNTHS);
  return (1);
}

int main(int argc, char **argv)
{
  pid_t     pids[MAX_SYNTHS + 1];
  unsigned  synths;

  pids[0] = 1;
  if (argc == 1)
    synths = DEFAULT_SYNTHS;
  else if (atoi(argv[1]) > MAX_SYNTHS)
      return usage();
  for (int i = 0; i < synths; i++)
    if (pids[i])
      pids[i] = launch_process("amsynth/build/amsynth", "-x", 0);
  pids[synths] = 0;
  sleep(2);
  launch_process("cli/cli", 0);
  watcher(&pids[0]);
  return (0);
}
