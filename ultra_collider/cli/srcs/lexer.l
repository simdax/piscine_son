%option noyywrap

%{
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

#include "parser.h"

//#include "rl.c"
int     count = 0;
// TODO : may be cool rules ?
//^({DIGIT}|1[0-5])$ yylval.ival = (unsigned char)atoi(yytext); return CHANNEL;
%}

DIGIT   [0-9]

%%

{DIGIT}+  yylval.ival = (unsigned char)atoi(yytext); return INT;

j       yylval.ival = 0; return INT;
k       yylval.ival = 1; return INT;

\n      ++count; return NEWLINE ; 
\t      return TAB;
scan    return SCAN;
c       return CONNECT;
n       return NEW;
p       return PROGRAM;
s       return STOP;
q       return QUIT;
v       return VOLUME;
i       return INFO;
.       ;

%%
