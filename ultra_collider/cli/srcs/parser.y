%{
#include <stdio.h>
#include <rtmidi/RtMidi.h>
#include "midi.h"

extern int              yylex();
extern int              yyparse();

void                    yyerror (const char *s);
void                    presentation(void);

typedef unsigned char   note_t;
std::vector<note_t>     count_notes;
MidiNote                midiout;

%}

%union {
    unsigned char   ival;
    float           fval;
    char            *sval;
}

%token          STOP VOLUME PROGRAM INFO QUIT
%token          NEW CONNECT SCAN
%token          TAB NEWLINE
%token<fval>    FLOAT
%token<ival>    INT

%%

parse:
     line NEWLINE { return(0); } 
     | NEWLINE { return(0); } 
;

line:
    ints | INT { midiout.bourdon($1, 60); }
    | NEW INT { midiout.set_channel($2); }
    | PROGRAM INT { midiout.program($2); }
    | CONNECT INT { midiout.openPort($2);}
    | VOLUME INT { midiout.set_volume($2); }
    | INFO { midiout.get_info(); }
    | STOP { midiout.panic(); }
    | SCAN { presentation(); }
    | QUIT { exit(0); }
;

ints:
    INT { count_notes.push_back($1); } ints
    | INT {
        count_notes.push_back($1);
        midiout.melodie(count_notes);
        count_notes.clear();
    } 
;
// TODO : for this rule INT == INT
//int : INT {$$ = $1} | CHANNEL {$$ = $1} ;


