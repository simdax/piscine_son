#include <stdio.h>
#include <unistd.h>
#include "midi.h"

extern int yyparse();
extern MidiNote midiout;
extern int count;

void    presentation(void)
{
  printf("SALUT MON LOULOU!\n");
  print_info();
}

int     main(int, char**)
{
  presentation();
  while(1)
  {
    std::cout << midiout.string_prompt(); 
    yyparse();
    printf("\n");
  }
}

void    yyerror(const char *s)
{
  printf("%s in %d\n", s, count);
}


