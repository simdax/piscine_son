#include <rtmidi/RtMidi.h>
#include <sstream>
#include <unistd.h>
#include <vector>
#include <thread>
#include <map>

#ifndef MIDI_H
# define MIDI_H

class MidiOut
{
  RtMidiOut                       out;
  std::vector<unsigned char>      msg = {0, 0, 0};

  void    _msg(unsigned char a, unsigned char b, unsigned char c)
  {
    msg[0] = a;
    msg[1] = b;
    msg[2] = c;
    //            printf("sending : %d %d %d", a, b, c);
    out.sendMessage(&msg); 
  }
  public:

  unsigned char                   channel = 0;

  void    openPort(unsigned char _port)
  {
    if (!out.isPortOpen())
    {
      try { 
        out.openPort(_port); 
        set_volume(10);
      } catch (RtMidiError &err) {
        std::cout << err.getMessage() << std::endl;
      }
    }
    else
      std::cout << "reconnecting" << std::endl;
  } 
  void    program(unsigned char program)
  {
    std::vector<unsigned char> mesg;

    mesg.push_back(192);
    mesg.push_back(program);
    out.sendMessage(&mesg);
  }
  void    set_volume(unsigned char volume)
  {
    _msg(176 + channel, 7, volume);
  }
  void    bourdon(unsigned char note, unsigned char velocity)
  {
    _msg(144 + channel, note, velocity);
  }
  void    note(unsigned char note, unsigned char velocity, unsigned dur = 500)
  {
    _msg(144 + channel, note, velocity);
    usleep(dur);
    _msg(128 + channel, note, velocity);
  }
  void    panic()
  {
    _msg(176 + channel, 120, 0);
  }
};

typedef unsigned char note_t;
typedef unsigned char channel_t;
typedef std::vector<note_t>                 melodie_t;
typedef std::map<int, melodie_t>            melodies_t;
typedef std::map<int, pthread_t>            proc_t;
typedef std::map<unsigned char, MidiOut>    channels_t;

class MidiNote 
{
  protected:
    //std::unique_ptr<MidiOut>        out;
    MidiOut                         *out;
    channels_t                      outs;
    std::vector<unsigned char>      msg = {0, 0, 0};

    int                             allocator = 0;
    proc_t                          processes; 
    melodies_t                      melodies; 

    std::vector<note_t>             scale;
    unsigned char                   root = 0;
    unsigned char                   octave = 5;
    unsigned                        period = 4000000;

  public:
    int                     current = 0;
    char                    port = -1;
    MidiNote ()
    {
      set_scale();
    }
    std::string string_prompt()
    {
      std::ostringstream   ss;

      ss << "[port:chan:mel][" <<
        (port < 0 ? std::string("%") : std::to_string(port)) << ":" <<
        get_channel() << ":" <<
        (current == 0 ? std::string("%") : std::string("mel")) <<
        "]";
      return ss.str();
    }
    void    set_channel(unsigned char c)
    {
      out->channel = c;
    }
    std::string get_channel()
    {
      if (out)
        return (std::to_string(out->channel))
          return (std::string("%"));
    }
    void    openPort(unsigned char _port)
    {
      out = &outs[_port];
      out->openPort(_port);
      port = _port;
    }
    void    set_scale(unsigned steps = 12, unsigned max = 2, 
                      unsigned min = 1, unsigned maxMins = 2)
    {
      bool wasMin = false;
      scale.clear(); 
      scale.push_back(0);
      while (steps)
      {
        if (rand() > 0.5 && maxMins && !wasMin)
        {
          scale.push_back(scale.back() + min);
          steps -= min;
          wasMin = true;
          --maxMins;
        }
        else
        {
          steps -= max;
          scale.push_back(scale.back() + max);
          wasMin = false;
        }
      }
    }
    void    set_volume(unsigned char volume)
    {
      out->set_volume(volume);
    }
    void    bourdon(unsigned char note, unsigned char velocity)
    {
      out->bourdon(12 * 3 + root + scale[note % scale.size()], velocity);
    }
    void note(unsigned char note, unsigned char velocity, unsigned dur = 500)
    {
      out->note(12 * octave + root + scale[note % scale.size()], velocity, dur);
    }
    void program(unsigned char program) 
    {
      out->program(program); 
    }
    void    get_info()
    {
      std::cout << "get process" << std::endl;
      for (auto const& process: processes)
        std::cout << process.first << std::endl;
    }
    void    panic()
    {
      for (auto &process: processes)
        pthread_cancel(process.second);
      out->panic();
    }
    void    melodie(melodie_t notes)
    {
      current = ++allocator;
      melodies[allocator] = notes;
      std::thread t([this](unsigned char port, melodie_t notes){
        while (true) 
        {
          unsigned p;
          p = (period) / notes.size();
          for (auto note:notes)
            outs[port].note(40 + note, 50, p); 
        }
      }, port, notes);
      processes[allocator] = t.native_handle();
      t.detach();
    }
};

int     print_info();

#endif
