# branche son a 42:

![](assets/music_branch.png)

---

  La decouverte
  ==

  1. Rush : Sine City => decouvrir PortAudio
---
  * en C, 
  * a la norme => sans le callback
  * bare bone => "ecrire" dans les bafles
  ![](assets/code_buffer.png)
---
  * comprendre de loin les bases
  * on utilise un sinus pour un 'vrai' son
  * du hasard pour du 'bruit'
  * l'idee de table d'onde comme approche naive des timbres
  ![](assets/code_tables.png)
---
  * avec potentiellement une petite manipulation
  * variation de frequence et d'amplitude 
  * gestion de la polyphonie

---

  Les bases theoriques necessaires
  ==

  <img id=graph1 src="assets/graph.svg"/>
  <script>

  </script>

---

#partir d'un son enregistre que l'on modifie

  <img src="assets/real.jpg" width=200/> ----->
  <img src="assets/La_Gioconda.jpg" width=200/>


---

  SAMPLES + FILTRES
  3.  Projet 2: instagr'Hann => recoder un media player
  
  L'intérêt étant surtout dans les filtres qu'on pourra lui appliquer
  * basiques : reverb;
  * plus interessant : biquad Filter (band pass, highpass, notch etc.);
  * plus complexe : speed & pitch shifter par synthese granulaire

---

#Partir de la theorie pour reproduire

  <img src="assets/fake.jpg" width=200/> ----->
  <img src="assets/La_Gioconda.jpg" width=200/>

---

  Les sous-basements theoriques mathematiques
  2.  Projet 1: Shaped Art Tones => mon premier synthetiseur
  * Pourquoi relie-t-on les fonctions dites sinusoidales avec l'idee du cercle ?

---

![](assets/shapes.gif)

  * Qu'est-ce que ca fait si mon cercle etait une autre figure ?

---

![](assets/heart.gif)

<audio src="assets/polygon_samples/classic.mp3" controls />
<audio src="assets/polygon_samples/acid.mp3" controls />
<audio src="assets/polygon_samples/bass.mp3" controls />
<audio src="assets/polygon_samples/pads.mp3" controls />

---

  * Qu'est-ce que ca fait si ma figure etait en mouvement ?
  * Qu'est-ce que ca fait si la maniere dont ces parametres vibrent changent etait eux meme des cercles ?

![](assets/agario.jpeg)

  L'idee est de construire un landscape de "figures sonores"

---

  Les technologies
  ==

---

  1. C / C++
  * Le gros leader hyper hype -> JUCE
  * VST, compatible avec tout
  * certainement le langage le plus adequat a un dernier projet
---
  2. Les langages de script ou affilié
  * Max
  * SuperCollider
  * Faust
  * Peut-être pour le Rush ?
---
  3. Une petite chose pas tres connue, que certains appellent le Ou ai be ?
  * Web Audio API
  * Web Assembly
  * [Web audio modules](https://www.webaudiomodules.org/wamsynths/)
  * [Une grosse DAW bien dodue](https://app.ampedstudio.com/)
---
  Les gros projets bien puissants
  ==

  4. Projet 3: Un bon game, parce qu'on aime rigouler


  Comme on parle de zizik, on va partir d'une seule contrainte: *que la bande-son soit donne par l'utilisateur.*

  Il faut realiser un jeu, avec un gameplay 
  prenant comme une input d'un cote les inputs utilisateurs, mais aussi une sound-track, qui doit faire partie 
  du gameplay.

---
  * En gros dans l'idee, il faut qu'il y ait un algorithme d'analyse => onset detection et peut-etre beat tracking

---

Mentions *satis bene*
  * Live coding
  * Visualisation
  * Musique generative
  * Les syntheses a modele physique
  * faire un "instrument virtuel"
  * la synthese vocale
  * la spatialisation
