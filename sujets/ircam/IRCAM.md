Chère Sophie Viger,

  J'allais écrire un mail à la pédago pour faire un plan d'attaque de l'IRCAM,
  quand tout soudain Arthur Lanter a qui je disais bonjour en passant m'explique que
  quelque chose se trame déjà. Après un bref échange, je comprends effectivement
  assez vite que vos antécédants en matière musicologique expliquent largement
  que vous ayiez déjà entrepris depuis un certain temps un rapprochement avec
  l'institut.

  Je m'occupe de la conception d'une branche son que j'espère pouvoir voir
  fleurir au sein de 42. Si vous le permettez, je vous joins le document que je
  comptais préparer pour la pédago, dans l'idée que tout restait à faire pour
  préparer une collaboration avec l'IRCAM, ce qui en fait est toujours le cas, mais
  votre participation nous inspire à tous. Ce document de manière je dois bien le reconnaître
  assez cash ce qu'il me semblait possible/intéressant de mener comme type
  d'échange.

  Vous imaginez bien que je serai au plus haut point intéressé par votre retour
  sur le document, et à globalement la poursuite de l'effort que nous menons
  finalement ensemble.


  /////////


  Voici un document qui vient en complément de la conception d'une branche sonore
  à 42. Un partenariat avec un institut aussi prestigieux et internationalement
  reconnu comme l'IRCAM serait évidemment une chance incroyable pour 42, pour la
  simple raison que l'IRCAM est tout sauf tourné vers l'extérieur. Au fond, à
  première vue, rien ne pousserait l'IRCAM à se tourner vers 42 car, même si la
  matière informatique semble commune, rien dans la pratique ne rapproche les
  deux écoles, et ceci au fond aussi bien du point de vue de la pédagogie que de
  la manière d'approcher le code même.

# Les missions de l'IRCAM

  C'est un institut qui a trois types de mission, que je résumerai par tranche
  d'âge:

  Je joins ici en image un document qui résume les trois points dont l'IRCAM se
  dit elle-même l'organisatrice.

  * Envers les plus jeunes, une mission d'éducation artistique. Cela consiste à
  faire des ateliers, des créations collectives d'oeuvres avec des jeunes, faire
  visiter la chambre anéchoique aux lycéens, etc. 

  * Une formation professionelle, dans le domaine du design sonore, de
  l'ingénieurie. Elle consiste essentiellement à former aux outils que l'IRCAM
  créée elle-même depuis maintenant des années, et dont MAX/MSP et sa
  programmation graphique constitue la pierre angulaire. Ce dernier est de
  facto le symbole de l'IRCAM qui l'a rendu assez cool par rapport aux
  logiciels dits music V qui étaient textuels et bien unfriendly comme on
  l'aime quand on est true UNIX avec une longue barbe.

  * Un département de recherche universitaire, avec d'un côté la science humaine
  musicale avec des partenariats avec la musicologie de Paris IV et le CNSM, et
  de l'autre la science dure avec des partenariats avec Marie Curie et
  ParisTech-Telecom. C'est le coeur spirituel de l'IRCAM, celui qui fait les
  festivals de musique sur France Musique tard le soir quand les enfants sont
  couchés.

  Les trois types de mission et de public sont susceptibles de donner lieu à des
  collaborations.  Pour les plus jeunes, 42 a déjà des initiatives similaires.
  Rien n'empêcherait les écoles qui viennent déjà faire les startup4kids de
  descendre jusqu'à Châtelet pour une spéciale son.

  Pour les deux autres points, 42 se trouve à un mi-chemin. La technicité et le
  niveau de compétence nécessaires à la réussite d'un cursus à 42 n'a rien à
  envier à un cursus supérieur de l'IRCAM, même si la distinction culturelle
  n'est évidemment pas la même. 42 se trouve en fait vraiment à un point aveugle
  de l'IRCAM, celui des `doers' là où l'IRCAM réfléchit (beaucoup). 

  À bien y réfléchir, le mélange
  est en fait improbable. L'image qui collerait le plus à cette
  rencontre, la plus intéressante pour envisager la collaboration avec l'IRCAM,
  c'est plus du côté de l'histoire de la belle et le clochard que celui d'une
  FUSAC USBC-GoldmanSachs; la belle qui fait des manières mais a une bonne éducation et le clochard
  céleste qui n'a rien d'autre que son bagout et son talent, ce dont par ailleurs
  il ne manque pas (c'est nous les clochards).

# Par ailleurs, 

  L'IRCAM n'a en fait plus vraiment le prestige qu'il avait lorsque Boulez régnait
  tout puissant sur la création musicale savante de France et de Navarre.

  Si vous vous étiez endormis au milieu de mon premier paragraphe, c'est ici qu'il
  faut se réveiller parce que c'est mon idée clef. C'est ici aussi que l'on
  ouvre les balises

  `<pied dans la fourmilière>`

  *Ce qui est drôle, c'est que les deux écoles, l'IRCAM et 42, viennent en fait d'une même pensée à la fois
  libertaire et structuralistes des années 60. Celle de dire que l'ordinateur
  était le meilleur ami de l'homme, de la connaissance et de l'art, et donc l'outil
  du turfu.*

  42 penchant plutôt du côté de la première option et l'IRCAM de la seconde, mais
  au fond, comme pour Bacchus et Apollon, l'ascendance est commune. Mais, concrètement dans les deux cas, la technologie du XXeme
  siècle ne permettant en fait pas à ces deux écoles de fleurir véritablement,
  42 a attendu 2013 pour ouvrir, avec sa grosse fibre et ses gros macs, et
  l'IRCAM a décidé de ne pas s'ouvrir du tout, et de travailler dans le secret,
  même si cela n'est évidemment pas reconnu comme tel.

  `</pied dans la fourmilière (mais en fait continuons un peu)>`

  L'IRCAM, et ce n'est en fait plus vraiment polémique de dire cela, semble avoir raté une grande
  partie du tournant que l'explosion grand public de l'informatique apportait:

  * Qu'en est-il des logiciels de création autre que Max ? Dans combien de temps ce n'est plus Max mais bel
  et bien Ableton patché avec Max que l'on y apprendra, voire même tout simplement un autre logiciel?
  * Qu'en est-il de la reconnaissance de partition ? Qu'en est-il des synthétiseurs absolument réalistes?
  * Qu'en est-il de l'IA ? Mais de toute facon quelle type de musique produirait-elle, sachant qu'elle sort de l'IRCAM ? 
  * Pourquoi les gens qui sortent de l'IRCAM n'y restent pas, mais partent directement chez Deezer
  ou Spotify, quand même ceux qui apportent le plus comme Francois Pachet sont purement et simplement
  des francs-tireurs n'y ayant jamais mis les pieds?
  * Mais en fait, quel impact réel cet institut a vraiment?

  Toutes ces critiques sont évidemment un peu chargées, mais la réalité est bel et bien
  plutôt que l'IRCAM refuse tout simplement de considérer ces points comme de véritables problèmes. C'est
  maintenant un lieu de recherche semblable à une boîte noire, contenant certainement de grandes vérités
  qui nous échappera peut-être à jamais.

  D'une certaine maniere, 42 est l'opposée total.

  42 est sexy, est un truc de jeunes et ne prétend pas spécialement formé à quoi que
  ce soit d'autre qu'à un développement personnel, une rigueur permettant de devenir totalement non
  specialiste, et tout à fait adapatable au grand méchant marché.

  Conclusion: y a un deal possible

# CONCRETEMENT

  Pour 42 c'est aussi une merveilleuse marque pour la débarasser de peut-être son
  dernier péché originel qui serait que oui c'est du haut niveau, mais c'est quand 
  même pas ingénieur, ce qui dans la pratique, nous semble faux à généraliser pour tous
  les étudiants.

  TL;DR
  Mon idée enfin, concise et simple : il faut faire venir l'IRCAM à 42. Faire aller 42 à l'IRCAM
  est totalement inutile et de toute facon, elle., mais au fonc ca c'est
  MOINS important

  l'IRCAM sera moyennement chaude => il faut montrer cela comme un challenge, une
  sorte de stage d'ete pour compositeur/futur technicien dans le monde
  merveilleux de la saleté. 

# le reste en chiffres et noms 

  1. les gens
  ATIAM => Moreno Andreatta coordinateur : donc en charge de l'ouverture du
  bordel Bertrand David: traitement du signal Benoit Fabre => trop pointu
  Emmanuel Saint-James => trop musical

  1.  des chiffres
  master 18 élèves, acoustique doctorat 5.
  80 élèves en formation professionelle.

  1. rappel des missions publics:
  * compositeurs
    * gros logiciels maison faits main veneres
    * ouvrir de nouvelles methodes, voire de nouveaux concepts
  * stagiaires "formation professionnelle"
    * se former aux gros logiciels deja presents
  * ateliers de lyceens
    * formation generale
  * les plus: 'design sonore' (?)

  1. research areas:
  musical material, form and meaning => NOP instruments,
  performers => BOF voice synthesis => NOP computer-aided orchestration => NOP
  interactivite => NOP musical material, form and meaning => NOP
  augmented instruments, sound spatialization, 
