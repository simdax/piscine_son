# Shape Art Tones

## Preambule

Le _Codex Chantilly_ n'est pas un manuel destiné à accompagner les glaces à la vanille. Le _Codex Chantilly_ n'est pas une nouvelle campagne Warhammer opposant les Eldars noirs contre le chaos  Le Codex Chantilly est un manuscrit du XIVe siècle regroupant des pièces musicales dans le style de l'*ars subtilior* ou, pour les quelques malandrins qui ne parlent pas latin, "l'art plus subtil". "Plus subtil que quoi ?" Tout simplement plus subtile que ce que les gens faisaient avant, la grosse musique du Moyen-âge, qui était bien trop bourrine pour les gens `bien nés`, si vous voyez ce que je veux dire. 

Il y a de fortes chances que ce style de musique du Moyen Âge de la fin du XIVe siècle, située entre l'ars nova (1310-1377) et l'école franco-flamande (1420-1600) contenant 112 pièces de compositeurs, parmi lesquels Trebor, Jacob Senleches, Solage ou encore, excusez du peu, le `famous` Baude Cordier, vous paraisse plus obscur encore que votre groupe de death-trans norvégien préféré que vous êtes pourtant déjà le seul à connaître. Mais il y a pourtant une raison pour laquelle vous devez connaître le _Codex Chantilly_. Car si la musique quelle contient est un peu moins à la mode aujourd'hui, le manuscrit qui parmi les personnes de goût reste incontournable reste la partition de la chanson au titre il faut l'avouer fort bien trouvé, _Belle, Bonne, Sage_. Tout un programme. 

![amour courtois](../ressources/images/coeur_chantilly.jpg)

Fun fact, en-dehors de considérations musicales, cette partition, en plus d'être comme vous l'avez remarqué hyper relou à déchiffrer pour les musiciens devant la jouer (heureusement il y en a peu), est aussi une des premières représentation en Europe d'un coeur à cette manière dite cardioïde. Car, pas besoin de vous faire un dessin, un vrai coeur au sens biologique du terme, ne ressemble pas du tout à cela. Même sur votre lit d'hôpital, il y a peu de chances qu'il ne ressemble à cela. C'est un symbole. Un truc que les artistes utilisent pour pouvoir faire n'importe quoi. La preuve.

![phrase en bliss](../ressources/images/bliss.png)

\newpage

## Introduction

En cherchant l'introduction de ce sujet, une citation que j'attribue à Léonard de Vinci m'est revenue que je n'arrive malheureusement plus à retrouver. En gros, cela faisait : "La musique a ce désavantage sur sa grande soeur la peinture de disparaître à peine apparue". Mais comme je n'arrive pas à la retrouver, j'en mets une équivalente de Miles Davis pas trop mal : "la véritable musique est le silence et toutes les notes ne font qu'encadrer ce silence". Il va sans dire que répondre à la question "qui est l'auteur de la première citation ?" revient évidemment à valider ce projet avec la note maximale (et un petit bisou en plus).

La question de la fixation du son a évolué fortement depuis les premières partitions du XIVeme. Aujourd'hui encore, la notation musicale reste sujette à de nombreuses expérimentations. Si aujourd'hui, le silicone et autres bandes magnétiques ont fait leurs preuves pour enregistrer le réel, il reste encore beaucoup à faire pour fixer l'irréelle, pour écrire l'idée du son et la modifier tel le corps d'un amant soumis qui nous obéirait au doigt et à l'oeil. 

Il va falloir se pencher un peu sur ce cas, et rétablir la Loi dans ce monde de brutes.

<!--
![phénomènes discrets bruyants](../ressources/images/death_waltz.jpg)
-->

\newpage

## Objectifs

L'objectif de projet est de construire un synthétiseur d'ondes polygonales. Si le premier projet de la branche sonore vous demandait de reproduire un ensemble de notes similaires, il s'agira maintenant de se concentrer sur la variation même du timbre, et sa description la plus précise. Au final, votre synthétiseur devra être capable de reproduire potentiellement n'importe quelle fonction périodique, sans mettre un milliard d'années.

![amour discourtois](../ressources/images/coeur_sin.gif)

\newpage

## Consignes générales

* Ce projet ne sera corrigé que par des humains. Vous êtes donc libres d’organiser
et de nommer vos fichiers comme vous le désirez, en respectant néanmoins les
contraintes listées ici.
* L’exécutable doit s’appeller shape_art_tones.
* Vous devez rendre un Makefile.
* Votre Makefile devra compiler le projet, portaudio et doit contenir les règles habituelles. Il
ne doit recompiler le programme qu’en cas de nécessité.
* Si vous êtes malin et que vous utilisez votre biliothèque libft,
vous devez en copier les sources et le Makefile associé dans un dossier nommé
libft qui devra être à la racine de votre dépôt de rendu. Votre Makefile devra
compiler la librairie, en appelant son Makefile, puis compiler votre projet.
* Votre projet doit être à la Norme.
* Vous devez gérer les erreurs de façon raisonnée. En aucun cas votre programme
ne doit quitter de façon inattendue (segmentation fault, bus error, floating point
exception, etc...).
* Dans le cadre de votre partie obligatoire, vous avez le droit d’utiliser les fonctions
suivantes :
  * open, read, write, close
  * malloc, free
  * perror, strerror
  * exit
  * Toutes les fonctions de la lib math (-lm et man 3 math)
  * Les fonctions de la bibliotheque portaudio : Pa_Initialize, Pa_GetErrotText, Pa_getLastHostError,Pa_GetDefaultOutputDevice, Pa_GetDeviceInfo, Pa_openStream, Pa_StartStream, Pa_writeStream, Pa_StopStream et Pa_CloseStream,
  * Vous avez l’autorisation d’utiliser d’autres fonctions dans le cadre de vos bonus, à
condition que leur utilisation soit dûment justifiée lors de votre évaluation. Soyez
malins.
* Vous pouvez poser vos questions sur le forum, Slack, etc.

## Partie obligatoire

Votre synthétiseur devra avoir un comportement *live*. À l'instar des claviers bontempi, taper sur une note que vous aurez défini vous-même devra faire un son.

Votre synthétiseur devra pouvoir reproduire n'importe quel polygone, n'importe quelle forme géométrique correspondant à un ensemble de points reliés dans l'espace sous la forme d'une onde sonore.

Votre lecture des polygones devra pouvoir boucler sur un ou plusieurs segments.

Votre synthétiseur devra pouvoir chaîner ses ondes à la manière d'expressions dans une équation.

Votre synthétiseur devra pouvoir enregistrer ses sons sous forme de 'presets' loadables à tout moment. Votre correcteur devra pouvoir repartir avec une description exacte des sons qu'il aura pu créer sur votre synthétiseur.

## Bonus

Une foultitude de bonos s'ouvrent a vous. En voici un échantillon:
 
* generateurs non periodique,
* UI en tout genre, représentation et modification *live* des polygones,
* lecture directe depuis n'importe quel device MIDI,
    * modification des notes,
    * système de MIDI learn pour la modification des autres paramètres,
* possibilité de connecter votre synthétiseur à votre séquenceur *sine_city*,
* etc, laissez place à votre imagination! 
