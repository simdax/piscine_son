# Sine City

## Preambule

La synthèse sonore est l'art de faire des bips avec un ordinateur. La philosophie hésitant toujours quant à la définition exacte d'un bip, seule la pratique commune nous apprendra que la vibration périodique d'un corps (solide ou membrane) quelconque (bois, plastique, cuivre, joues, ventres ou fesses dodues ainsi que soundsytem de luxe ensemble subwoofer/boomer/tweeter n'en étant qu'un echantillon non-exhaustif) sous l'impulsion d'une frappe, d'un mediator, d'un fouet trouvé pas cher sur le Bon Coin catégorie "litterature érotique", d'une grande poussée d'air ou d'un signal électrique fournit une base perceptive des plus satisfaisantes a la définition bippesque.

![définition bippesque](../ressources/images/Roadrunner.png)

La manière de faire vibrer une membrane et d'en tirer la perception musicale la plus joyeuse est un art difficile. Potentiellement, toute vibration de l'air par les moyens sus-cités produiront, depuis que l'air est air et que la Physique est Physique, un bruit. Or, la transsubstantiation du bruit en bip, qui fera notre premier bonheur pour aujourd'hui, accomplie dans toute sa mansuétude par le grand Sinus, sera votre première etape pour trouver la voie vers l'Harmonie.

# Introduction

Ce sujet est une initiation à la MAO bas-niveau. Oubliez tous les logiciels de traitement du son présents sur la console de mixage de votre grand Oncle ou autres Digital Audio Workstations piratées à la va-vite permettant au premier hipster venu de Daft Punkiser à vil prix, il s'agira maintenant de s'attaquer à la racine de la production audio, au coeur de la synthèse sonore en participant aux rites initiatiques de la divine Déesse P. (appelée aussi parfois par les païens Digital Signal Processor). Rien de ce qui ne sortira de la bouche de vos haut-parleurs ne vous sera dorénavant etranger, et, espérons-le, étrange (même si l'on pourra en douter dans un premier temps).

Ce sujet propose d'aborder une technique de synthèse basique et usuelle que l'on nomme couramment dans les dîners mondains _synthèse par table d'onde_. Cette dernière consiste à définir par vous-même un ensemble de `tables d'onde`, chacune étant techniquement la fonction d'une somme infinie de sinus dans le temps, plus trivialement une manière de vibrer, produisant ainsi un "timbre", un "type de son" particulier. Ce sont ces tables d'onde qui seront lues par votre programme pour faire vibrer vos haut-parleurs et donner bon goût à l'air que vous respirerez par les oreilles.

L'ordinateur accomplira lui-même les vibrations à l'aide d'une bibliothèque externe, `portaudio`. Cette bibliothèque vous permettra de découvrir les joies d'une *librairie tierce-partie* que vous devrez compiler par vous-mêmes, et qui rendra par la force du grand Open Source votre project Compatible et Universel, Apostolique et Romain. Amen.

## Objectifs

Il s'agit d'imaginer un dispositif permettant à n'importe quel cerveau malade (vous n'aurez pas de difficulté à en trouver) ou génie de la Muse (plus difficile) d'exprimer, sur une quelconque plateforme pouvant compiler du C, un ensemble de sons variant selon trois paramètres obligatoires décrits ci-dessous, ainsi que n'importe quelleautre variable qui vous semblera digne de présenter à une Humanité en attente de Beauté une Joie potentielle.

Dit plus simplement, vous devrez rendre un binaire permettant de lire de manière minimale un ensemble de notes jouées successivement et potentiellement de manière simultanée, a.k.a une partition musicale.

* Ce que l'on entend par 'note' décrira la manière immensément bonne dont une membrane vibre quand elle le fait du même mouvement que le fait le tracé d'un cercle, ou tout autre forme présente dans le grand livre de la Nature et de la *Mathesis Universalis*,
* Ce que l'on entend par 'de manière minimale' insiste sur les caractéristiques basiques d'un son, à savoir :

  * sa hauteur, (son pitch, sa fréquence),
  * sa force, (son amplitude, son volume),
  * le temps qu'il vous fera jouir, (sa durée).

* Ce que l'on entend par 'potentiellement simultanée', c'est que vous devrez trouver un moyen d'empiler les bips afin de rendre de manière fidèle une pratique ignorée de 95% de la planète mais qui fait les délices des amateurs de contrepoint Renaissance et de Mireille Matthieu (95% du reste) : la polyphonie, ou l'art de faire résonner des bips aux caractéristiques différentes au même point de l'Espace-Temps.
* Ce que l'on entend par 'partition musicale' désigne communément un bout de papier ou plus récemment un fichier numérique, qui, lu et interprété de manière adéquate ressemblera à quelque chose que Beethoven ou Britney Spears auraient pu produire (selon l'époque et le taux d'alcoolemie).

## Consignes générales

* Ce projet ne sera corrigé que par des humains. Vous êtes donc libres d’organiser
et de nommer vos fichiers comme vous le désirez, en respectant néanmoins les
contraintes listées ici.
* L’exécutable doit s’appeller sine_city
* Vous devez rendre un Makefile.
* Votre Makefile devra compiler le projet, portaudio et doit contenir les règles habituelles. Il
ne doit recompiler le programme qu’en cas de nécessité.
* Si vous êtes malin et que vous utilisez votre biliothèque libft,
vous devez en copier les sources et le Makefile associé dans un dossier nommé
libft qui devra être à la racine de votre dépôt de rendu. Votre Makefile devra
compiler la librairie, en appelant son Makefile, puis compiler votre projet.
* Votre projet doit être à la Norme.
* Vous devez gérer les erreurs de façon raisonnée. En aucun cas votre programme
ne doit quitter de façon inattendue (segmentation fault, bus error, floating point
exception, etc...).
* Dans le cadre de votre partie obligatoire, vous avez le droit d’utiliser les fonctions
suivantes :
  * open, read, write, close
  * malloc, free
  * perror, strerror
  * exit
  * Toutes les fonctions de la lib math (-lm et man 3 math)
  * Les fonctions de la bibliotheque portaudio : Pa_Initialize, Pa_GetErrotText, Pa_getLastHostError,Pa_GetDefaultOutputDevice, Pa_GetDeviceInfo, Pa_openStream, Pa_StartStream, Pa_writeStream, Pa_StopStream et Pa_CloseStream,
  * Vous avez l’autorisation d’utiliser d’autres fonctions dans le cadre de vos bonus, à
condition que leur utilisation soit dûment justifiée lors de votre évaluation. Soyez
malins.
* Vous pouvez poser vos questions sur le forum, Slack, etc.

## Partie obligatoire

![syntaxe basique](../ressources/images/sinecity_examples.png)


Ce fichier sera constitué par un ensemble de 'voix'.

Une 'voix' est une ligne de header suivi d'un nombre non-prédéfini de lignes représentant des notes. (Ce sera, pour les musiciens, et les autres, un équivalent de la portée dans la notation musicale traditionnelle.)

Chaque header devra commencer par un '#' et permettra de configurer des options qu'il sera libre à vous de déterminer. 

Chaque ligne suivant ce header correspondra à une note. Cette ligne devra décrire obligatoirement les trois paramètres basiques du son, dans l'ordre suivant : hauteur, durée et volume.

Plusieurs lignes successives seront jouées de manière successive, chaque note suivant l'autre.

Si le fichier comporte plusieurs voix, elles seront alors jouées simultanément.

## Bonus

Une foultitude de bonos s'ouvrent a vous. En voici un échantillon:
  
* différents types de timbres (sinus, triangulaire, presence d'harmoniques, bruit, etc.) 
* gestion basique d'enveloppe : ASR ou ADSR,
* lecture directe de fichiers MIDI,
* implementation de SoundFonts (SF2 ou autre),
* syntaxe personnelle dans les headers en plus de la partie obligatoire:
    * choix des unités pour le trio fréquence/durée/volume, et possible traitement 'solfégique', de l'utilisation des notes classiques du 'do' jusqu'au 'si' (avec l'octave) et la découpe temporelle du système "blanche-noire-croche-double",
    * utilitaire pouvant jouer directement des accords (notation classique de basse continue par degré `I ii V` ou américaine `C7b13sus`),
    * arpéggiateur, ou petite syntaxe permettant de préciser comment jouer une section d'accompagnement répétitive à la maniere d'un 'band-in-a-box', 
    * notations permettant de préciser ces répétitions, en imaginant des "grilles d'accords" ou des parcours permettant à la musique de se transformer comme si elle parcourait un labyrinthe,
* etc, laissez place à votre imagination! 
