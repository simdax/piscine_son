/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 16:45:05 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/27 16:51:04 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "string.h"
#include "printf.h"
#include "array.h"
#include "btree.h"
#include "ast.h"
#include "print.h"
#include "numbers.h"

int		add(int a, int b)
{
	return (a + b);
}

t_token	*tokenize(char *str)
{
	t_token *token;

	token = malloc(sizeof(*token));
	if (ft_isdigit_str(str))
		*token = (t_token){NUMBER, str, ft_atoi(str), 0, 0};
	else
		*token = (t_token){OPERATOR, str, 0, add, 1};
	return (token);
}

int		insertion(t_btree *tree, void *el)
{
	void	*tmp;
	t_token	*token;

	token = el;
	return (-1);
}

void	rm_skel(t_btree **node, int i)
{
	(*node)->data = ((t_token*)(*node)->data)->str;
}

int		main(void)
{
	char	*txt;
	char	**tokens;
	char	**cpy;
	t_tree	ast;

	ast.sort_f = insertion;
	ast.b_tree = 0;
	txt = get_file_content("test/2");
	ft_printf("getting text \t: %s", txt);
	tokens = ft_strsplit(txt, ' ');
	cpy = tokens;
	while (*tokens)
	{
		btree_insert(&ast.b_tree, tokenize(*tokens), insertion);
		++tokens;
	}
	btree_apply_level(&ast.b_tree, rm_skel);
	btree_print_str(ast.b_tree);
	ft_free_strsplit(cpy);
	free(txt);
	return (0);
}
