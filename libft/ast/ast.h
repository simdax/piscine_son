/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 16:49:50 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/27 16:49:51 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AST_H
# define AST_H

enum			e_token_type {
	NUMBER, OPERATOR
};

typedef struct	s_token
{
	e_token_type	type;
	char			*str;
	int				nb;
	void			*data;
	unsigned		priority;
}				t_token;

#endif
