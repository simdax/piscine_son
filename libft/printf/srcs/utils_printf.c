/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_printf.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 18:52:23 by scornaz           #+#    #+#             */
/*   Updated: 2018/04/14 12:14:44 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <inttypes.h>
#include "parser_printf.h"
#include "types_ca.h"
#include "prototypes_ca.h"
#include "libft.h"

static void		hydrate(t_num *a, t_flags *flags)
{
	a->left = !flags->minus;
	a->star = flags->star;
	a->padding = flags->width;
	a->precision = flags->precision;
	a->alternate = flags->hash;
	a->sign = flags->plus;
	a->zero = flags->zero;
	a->space = flags->space;
}

t_num			flags2print(va_list arg, t_flags flags)
{
	t_num		a;
	intmax_t	value;

	hydrate(&a, &flags);
	split_type(flags.type, &a);
	if (a.type == 'E')
	{
		va_arg(arg, void*);
		a.value = ft_strdup("");
		a.type = 's';
	}
	else if (ft_strchr("csSC", a.type))
		strings(&a, arg);
	else if (a.type == '%')
	{
		a.value = ft_strdup("%");
		a.type = 'c';
	}
	else
	{
		value = va_arg(arg, intmax_t);
		if (flags.esperluette)
						value = *(intmax_t*)value;
		parse_value(&value, &a);
	}
	re_orga(&a);
	return (a);
}

int			print_and_free(t_num **nums, char ***str, t_array *buffer)
{
	size_t	len;

	len = ft_strlen(**str);
	array_add(buffer, **str, len);
	free(**str);
	++(*str);
	print_arg(*nums, buffer);
	free((*nums)->value);
	free((*nums)->modifiers);
	++(*nums);
	return (0);
}
