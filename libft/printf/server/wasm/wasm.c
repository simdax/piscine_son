/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wasm.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/25 18:16:59 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/26 10:24:26 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
#include <time.h>
#include <emscripten.h>
#include <string.h>

int fib(int n) {
	if (n == 1) return 1;
	if (n == 2) return 1;
	return fib(n-1) + fib(n-2);
}

int	main()
{
	int		i;
		    time_t depart, arrivee;
			    time(&depart);

	i = 0;
	while (i < 40)
	{
		ft_printf("HELLO GROS WORLD : %d\n", fib(36));
		++i;
	}
	time(&arrivee);
	printf(" %f secondes.\n", difftime(arrivee, depart));
}
