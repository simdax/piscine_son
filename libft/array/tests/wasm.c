/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wasm.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 10:37:15 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/27 16:38:33 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../array.h"
#include "../../printf/includes/printf.h"

int		main(void)
{
	char	*ret;
	t_array *array;

	ret = get_file_content("lorem");
	ft_printf("%s", ret);
	free(ret);
	array = array_new_ints(10, 45, 134, 42, 3234, 54, 5, 6, 7, 8, 9);
	array_print(array);
	array_free(array);
	array = array_rand(1000);
	array_print(array);
	array_free(array);
}
