/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wasm.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 10:37:15 by scornaz           #+#    #+#             */
/*   Updated: 2018/10/12 18:33:28 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../array.h"

int		main(void)
{
	char	*ret;
	t_array *array;

	array = array_new_ints(2, 0, 1); 
	array_print(array);
	array_swap(array, 0, 1); 
	array_print(array);
	array_free(array);
}
