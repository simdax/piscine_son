/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 15:55:33 by scornaz           #+#    #+#             */
/*   Updated: 2018/10/12 15:08:35 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "array.h"
#include "../../printf/includes/printf.h"

static void	print(void *el, t_array *a)
{
	if (a->type_len == sizeof(int))
		ft_printf("%&d\n", el);
	else
		ft_printf("%p\n", el);
}

void		array_print(t_array *array)
{
	if (array->type_len == 1)
		ft_printf("%s", array->mem);
	else
		array_for_each(array, print);
}
