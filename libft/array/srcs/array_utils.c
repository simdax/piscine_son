/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/01 13:55:16 by scornaz           #+#    #+#             */
/*   Updated: 2018/10/12 19:37:06 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "array.h"

void		array_free3(t_array **array)
{
	array_free(*array);
	*array = NULL;
}

void		*array_find(t_array *array, int (*f)(void *el, void *data),
		void *data)
{
	unsigned	i;

	i = 0;
	while (i < array->cursor)
	{
		if (f(array->mem + array->type_len * i, data))
			return (array->mem + array->type_len * i);
		++i;
	}
	return (0);
}

void 		*array_pop(t_array *array)
{
	void	*ret;

	if (array->cursor)
	{
		ret = ft_malloc(array->type_len);
		ft_memcpy(ret, array_at(array->cursor - 1, array), array->type_len);
		array_nullify_at(array, array->cursor - 1, 1);
		--array->cursor;
		return (ret);
	}
	return (0);
}

void		*array_at(unsigned i, t_array *array)
{
	return (i >= array->cursor ? 0 :
			array->mem + array->type_len * i);
}

void		array_insert_at(unsigned i, t_array *array, void *data)
{
	void		*zero;
	unsigned	diff;

	if (i >= array->cursor)
	{
		diff = i - array->cursor;	
		zero = ft_memalloc(diff * array->type_len);
		array_add(array, zero, diff + 1);
	}
	ft_memcpy(array->mem + array->type_len * i, data, array->type_len);
}

void	array_swap(t_array *array, unsigned a, unsigned b)
{
	ft_memswap(array_at(a, array), array_at(b, array), array->type_len);
}

void	array_nullify_at(t_array *array, unsigned i, unsigned size)
{
	ft_bzero(array->mem + array->type_len * i, array->type_len * size);	
}
