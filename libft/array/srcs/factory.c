/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   factory.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 15:55:56 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/27 16:38:07 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "array.h"
#include "rng.h"

t_array		*array_new_ints(int nb, ...)
{
	t_array	*array;
	va_list	list;

	va_start(list, nb);
	array = varray_new_ints(nb, list);
	va_end(list);
	return (array);
}

t_array		*varray_new_ints(int nb, va_list list)
{
	t_array		*array;
	int			val;

	array = array_new(sizeof(int), nb);
	while (nb--)
	{
		val = va_arg(list, int);
		array_add(array, &val, 1);
	}
	return (array);
}

t_array		*array_range(int nb)
{
	t_array	*array;
	int		val;
	int		i;

	i = 0;
	array = array_new(sizeof(int), nb);
	while (i < nb)
	{
		array_add(array, &i, 1);
		i++;
	}
	return (array);
}

t_array		*array_rand(int nb)
{
	t_array	*instance;
	int		tmp;

	instance = array_new(sizeof(int), nb);
	while (nb--)
	{
		tmp = rng();
		array_add(instance, &tmp, 1);
	}
	return (instance);
}
