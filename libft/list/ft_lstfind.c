/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstfind.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/01 16:08:55 by scornaz           #+#    #+#             */
/*   Updated: 2018/04/14 12:27:53 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static  int basic_eq(void *a, void *b)
{
  return (a == b);
}

t_list	*ft_lstfind(t_list *el, int (*f)(t_list *el, void *cmp), void *cmp)
{
  while (el)
  {
    if ((!f && basic_eq(el->content, cmp)) || (f && f(el, cmp)))
        return (el);
    el = el->next;
  }
  return (0);
}

t_list	*ft_lstget(t_list *el, int index) 
{
  while (el && index--)
    el = el->next;
  return (el);
}

t_list	*ft_lstgetlast(t_list *el) 
{
  while (1)
  {
    if (el->next)
      el = el ->next;
    else 
      break;
  }
  return (el);
}
