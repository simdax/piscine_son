/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 16:46:17 by scornaz           #+#    #+#             */
/*   Updated: 2018/01/21 16:46:18 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	ft_lstdelone(t_list **alst, void (*del)(void*, size_t))
{
  if (*alst && del)
    del((*alst)->content, (*alst)->content_size);
  else if (*alst)
    free((*alst)->content);
  free(*alst);
  *alst = NULL;
}

void  ft_lstremove(t_list **head, t_list *elem, void (*del)(void*, size_t))
{
  if (*head == elem)
  {
    *head = elem->next;
    ft_lstdelone(&elem, del);
    return ;
  }
  while (*head && (*head)->next != elem)
    *head = (*head)->next; 
  if (*head)
        {
    (*head)->next = elem->next;
    ft_lstdelone(&elem, del);
  }
}
