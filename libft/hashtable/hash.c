/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 13:59:05 by scornaz           #+#    #+#             */
/*   Updated: 2018/06/11 15:00:29 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mem.h"
#include "ft_string.h"
#include "hash.h"
#include <unistd.h>
#include <stdio.h>

static int	hash_function(t_hash *self, char *key)
{
  int		  count;
  size_t	size;

  size = ft_strlen(key);
  if (!*key) 
  {
    write(2, "error\n", 6);
    return (0);
  }
   count = 0;
  while (size--)
    count += key[size];
 if (count > self->size)
  {
    printf("key size too long : %s\n", key);
    return (0);
  }
   return (count + 1);
}
  
unsigned long djb2(unsigned char *str)
{
  unsigned long hash = 5381;
  int c;

  while (c = *str++)
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

  return hash;
}

static unsigned long sdbm(unsigned char *str)
{
  unsigned long hash = 0;
  int c;

  while (c = *str++)
    hash = c + (hash << 6) + (hash << 16) - hash;

  return hash;
}

t_hash		*hash_new(size_t size)
{
  t_hash	*hash;
  t_tree  indexes;

  if (!(hash = ft_memalloc(sizeof(t_hash))))
    return (0);
  hash->mem = malloc(sizeof(void*) * size);
  hash->keys = indexes(SEARCH, 0);
  if (!hash->mem || !hash->keys)
    return (0);
  hash->size = size;
  hash->code = hash_function;
  return (hash);
}

void		*hash_add(t_hash *self, char *key, void *content, size_t size)
{
  unsigned	index;

  index = self->code(self, key);
  self->mem[index] = ft_memalloc(size);
  if (!index || !self->mem[index])
    return (0);
  ft_memcpy(self->mem[index], content, size);
  return (self->mem[index]);
}

void		*hash_get(t_hash *self, char *key)
{
  int	index;

  index = self->code(self, key);
  if (index)
    return (self->mem[index]);
  return (0);
}
