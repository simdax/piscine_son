/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 13:56:04 by scornaz           #+#    #+#             */
/*   Updated: 2018/06/11 14:59:33 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hash.h"
#include <stdio.h>
#include <unistd.h>

int		main(void)
{
	t_hash	*hash;

	hash = hash_new(50000);
	hash_add(hash, "un", "item number 1", 6);
	hash_add(hash, "truc", "item number 2", 6);
	hash_add(hash, "de", "item number 3", 6);
	hash_add(hash, "ouf", "item number 4", 6);
	printf("beuh %s %s %s %s", hash_get(hash, "un"), hash_get(hash, "fsa"),
		hash_get(hash, "de"), hash_get(hash, "fou"));
	return (0);
}
