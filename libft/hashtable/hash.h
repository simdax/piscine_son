#ifndef HASH_H
# define HASH_H

# include <stddef.h>

typedef struct s_hash
{
	int		  (*code)(struct s_hash *self, char *key);
	int		  *keys;
	size_t  size;
	void	  **mem;
}				      t_hash;

t_hash		*hash_new(size_t size);
void			*hash_get(t_hash *self, char *key);
void			*hash_add(t_hash *self, char *key, void *content, size_t size);

#endif
