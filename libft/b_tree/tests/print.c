/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 10:36:35 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/27 17:02:47 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../btree.h"

static void		p(t_btree **node, int index)
{
	int	*i;
   
	i = malloc(sizeof(int));
	*i = index;
	if (!(*node))
		*node = btree_new_node(i);
	else if (!(*node)->data)
		(*node)->data = i;
	if (index < 31)
	{
		(*node)->left = btree_new_node(0);
		(*node)->right = btree_new_node(0);
	}
}

int				main(void)
{
	t_btree *head = 0;
	t_array *data;

	btree_apply_level(&head, p);
	btree_print(head);
}
