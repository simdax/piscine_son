/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 10:26:06 by scornaz           #+#    #+#             */
/*   Updated: 2018/04/01 16:05:43 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BTREE_H
# define BTREE_H

# include <stdlib.h>

typedef struct    s_meta_tree
{
  int level;
  int rank;
}                 t_meta_tree;

typedef struct		s_btree
{
  struct s_btree	*left;
  struct s_btree	*right;
  t_meta_tree     *metadata;
  void		      	*data;
}       					t_btree;

typedef struct		s_iter_data
{
  int	level;
  int level_tmp;
  int rank;
}				        	t_iter_data;

typedef struct		s_tree{
  t_btree	*b_tree;
  int			(*sort_f)(t_btree *tree, void *data);
}				        	t_tree;

typedef enum {FLAT, SEARCH} e_tree_types;
typedef enum {AVL_LEFT, AVL_RIGHT} e_avl_rot_type;

t_tree		tree_new(e_tree_types type, int nb, ...);
t_btree		*btree_new_search(int nb, ...);
t_btree		*btree_new_flat(int nb, ...);
t_btree		*btree_new_node(void *data);
void			btree_apply_prefix(t_btree *root, void (*apply)(void *data));
void			btree_apply_infix(t_btree *root, void (*apply)(void *data));
void			btree_apply_postfix(t_btree *root, void (*apply)(void *data));
void			print_btree(t_btree *root);
void			btree_insert(t_btree **root, void *item,
            int (*cmpf)(t_btree*, void*));
int       btree_avl_insert(t_btree **root, void *item, int (*cmpf)(t_btree*, void*));
void      btree_right_rotation(t_btree **pivot);
void      btree_left_rotation(t_btree **pivot);
void			*btree_search_item(t_btree *root, void *data_ref,
            int (*cmpf)(void*, void*));
int				btree_level_count(t_btree *root);
void      btree_apply_level(t_btree **root,
            void (*apply)(t_btree **node, int));
void			btree_print(t_btree *root);
void	  	btree_print_str(t_btree *root);
int       *get_lvl_rank(int index);
int			  btree_calc_level(t_btree *root);

#endif
