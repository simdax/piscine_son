/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 10:27:07 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/27 17:04:56 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "btree.h"
#include "lst_includes.h"

void		btree_apply_prefix(t_btree *root, void (*apply)(void *data))
{
	if (!root)
		return;
	apply(root->data);
	btree_apply_prefix(root->left, apply);
	btree_apply_prefix(root->right, apply);
}

void		btree_apply_postfix(t_btree *root, void (*apply)(void *data))
{
	if (!root)
		return;
	btree_apply_postfix(root->right, apply);
	btree_apply_postfix(root->left, apply);
	apply(root->data);
}

void		btree_apply_infix(t_btree *root, void (*apply)(void *data))
{
	if (!root)
		return;
	btree_apply_infix(root->left, apply);
	apply(root->data);
	btree_apply_infix(root->right, apply);
}

void		btree_apply_level(t_btree **root, void (*apply)(t_btree **node, int index))
{
	t_list	*queue;
	int		index;

	index = 0;
	queue = ft_lstnew(&root, sizeof(root));
	while (queue)
	{
		apply(root, index);
		if (*root && (*root)->left)
			ft_lstaddlast(&queue, ft_lstnew(&(*root)->left, sizeof(t_btree)));
		if (*root && (*root)->right)
			ft_lstaddlast(&queue, ft_lstnew(&(*root)->right, sizeof(t_btree)));
		queue = queue->next;
		++index;
		if (queue)
			root = queue->content;
	}
	ft_lstdel(&queue, 0);
}
