/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 13:50:26 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/27 17:00:10 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "btree.h"
#include "string.h"
#include "printf.h"

t_btree			*btree_new_node(void *data)
{
	t_btree		*ret;

	if (!(ret = malloc(sizeof(t_btree))))
		return (0);
	ret->right = 0;
	ret->left = 0;
	ret->data = data;
	return (ret);
}

void		btree_insert(t_btree **root, void *item, int (*cmpf)(t_btree*, void*))
{
	if (!(*root))
		*root = btree_new_node(item);
	else
	{
		if (cmpf(*root, item) > 0)
			btree_insert(&((*root)->left), item, cmpf);
		else
			btree_insert(&((*root)->right), item, cmpf);
	}
}

void		*btree_search_item(t_btree *root, void *data_ref,
		int (*cmpf)(void*, void*))
{
	t_btree		*ret;

	if (!root)
		return (0);
	if (!cmpf(root->data, data_ref))
		return (root);
	if (root->left)
	{
		ret = btree_search_item(root->left, data_ref, cmpf);
		if (ret)
			return (ret);
	}
	if (root->right)
	{
		ret = btree_search_item(root->right, data_ref, cmpf);
		if (ret)
			return (ret);
	}
	return (0);
}
