/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   level.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 15:13:02 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/27 17:04:12 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "btree.h"
#include "../../list/lst_includes.h"
#include "../../printf/includes/printf.h"

static void		tree_level(t_btree *node, t_iter_data args, void *data,
		void (*apply)(void*, int level, int rank, void*))
{
	if (!args.level_tmp)
		if (node)
			apply(node->data, args.level, args.rank, data);
		else
			apply(0, args.level, args.rank, data);
	else
	{
		--args.level_tmp;
		args.rank *= 2;
		if (node && node->left)
			tree_level(node->left, args, data, apply);
		else
			tree_level(0, args, data, apply);
		++args.rank;
		if (node && node->right)
			tree_level(node->right, args, data, apply);
		else
			tree_level(0, args, data, apply);
	}
}

static void		btree_level_rec(t_btree *root, void *data,
		void (*apply)(void *item, int, int, void*))
{
	int		height;
	int		i;

	i = 0;
	height = btree_level_count(root);
	while (i <= height)
	{
		tree_level(root, (t_iter_data){i, i, 0}, data, apply);
		++i;
	}
}

static void		print(void *val, int level, int rank, void *opts)
{
	int		width;
	int		middle;
	int		space;
	char	val_str[10];

	width = 128;
	space = width / (1 << level);
	space = !rank ? space / 2 : space;
	if (!val)
	{
		val_str[0] = '.';
		val_str[1] = 0;
	}
	else if (opts)
		ft_memcpy(val_str, val, 10);
	else
		ft_sprintf(val_str, "%d", *(int*)val);
	ft_printf("%*s", space, val_str, level, rank);
	if (rank == ((1 << level) - 1))
		ft_printf("\n");
}

void			btree_print(t_btree *root)
{
	btree_level_rec(root, 0, print);
}

void			btree_print_str(t_btree *root)
{
	btree_level_rec(root, (void*)1, print);
}
