/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   avl.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 16:53:32 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/27 17:03:23 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "btree.h"
#include "printf.h"

void	btree_avl_balance(t_btree **node, int child, int count_child)
{
	if (child == 2 && count_child == 1)
		btree_left_rotation(node);
	else if (child == -2 && count_child == -1)
		btree_right_rotation(node);
	else if (child == 2 && count_child == -1)
	{
		btree_right_rotation(&(*node)->right);
		btree_left_rotation(node);
	} 
	else if (child == -2 && count_child == 1)
	{
		btree_left_rotation(&(*node)->left);
		btree_right_rotation(node);
	} 
}

int		btree_avl_insert(t_btree **root, void *item, int (*cmpf)(t_btree*, void*))
{
	int count;
	int count_child;

	if (!(*root))
		*root = btree_new_node(item);
	else
	{
		if (cmpf(*root, item) > 0)
			count_child = btree_avl_insert(&((*root)->left), item, cmpf);
		else
			count_child = btree_avl_insert(&((*root)->right), item, cmpf);
		count = btree_level_count((*root)->right) -
			btree_level_count((*root)->left);
		btree_avl_balance(root, count, count_child);
		return (count);
	}
	return (1);
}

void	btree_left_rotation(t_btree **pivot)
{
	t_btree *tmp1;
	t_btree *tmp2;

	tmp1 = *pivot;
	tmp2 = (*pivot)->right->left;
	*pivot = tmp1->right;
	(*pivot)->left = tmp1;
	(*pivot)->left->right = tmp2;
}

void	btree_right_rotation(t_btree **pivot)
{
	t_btree *tmp1;
	t_btree *tmp2;

	tmp1 = *pivot;
	tmp2 = (*pivot)->left->right;
	*pivot = tmp1->left;
	(*pivot)->right = tmp1;
	(*pivot)->right->left = tmp2;
}
