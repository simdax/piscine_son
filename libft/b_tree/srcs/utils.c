/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 16:55:07 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/27 17:00:25 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "btree.h"
#include "../array/array.h"
#include "../numbers/numbers.h"
#include <stdio.h>
#include <stdarg.h>

int				*get_lvl_rank(int index)
{
	static int	lvl_rank[2];
	int			i;

	i = 0;
	lvl_rank[0] = 0;
	lvl_rank[1] = 0;
	while (i < index)
	{
		++lvl_rank[1];
		if (lvl_rank[1] == 1 << lvl_rank[0])
		{
			++lvl_rank[0];
			lvl_rank[1] = 0;
		}
		++i;
	}
	return (lvl_rank);
}

static int		mstrcmp(t_btree *root, void *b)
{
	return (*(int*)root->data - *(int*)b);
}

static int		flat_insert(t_btree *root, void *data)
{
	return (root->right != 0);
}

static void		insert_callback(void *el, void *data, t_array *a)
{
	t_tree	*tree;

	tree = (t_tree*)data;
	btree_avl_insert(&(tree->b_tree), el, tree->sort_f);
}

t_tree			tree_new(e_tree_types type, int nb, ...)
{
	t_tree	tree;
	va_list	list;
	t_array	*data;

	tree.b_tree = 0;
	if (type == FLAT)
		tree.sort_f = flat_insert;
	else if (type == SEARCH)
		tree.sort_f = mstrcmp;
	va_start(list, nb);
	data = varray_new_ints(nb, list);
	va_end(list);
	array_for_each2(data, &tree, insert_callback);
	return (tree);
}
