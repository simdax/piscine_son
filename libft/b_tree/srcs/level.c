/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   level.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 17:00:15 by scornaz           #+#    #+#             */
/*   Updated: 2018/09/27 17:00:16 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "btree.h"

int			btree_level_count(t_btree *root)
{
	int	left_nb;
	int	right_nb;

	left_nb = 0;
	right_nb = 0;
	if (!root)
		return (0);
	if (root->right)
	{
		++right_nb;
		right_nb += btree_level_count(root->right);
	}
	if (root->left)
	{
		++left_nb;
		left_nb += btree_level_count(root->left);
	}
	return (right_nb > left_nb ? right_nb : left_nb);
}

