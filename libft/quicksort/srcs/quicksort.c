#include "quicksort.h"
#include "array.h"

static void   swap_int(int *a, int *b)
{
  int tmp;

  tmp = *a;
  *a = *b;
  *b = tmp;
}

static void   do_quick(int* start, int* pivot)
{
  int *less;
  int *cpy_start;

  cpy_start = start;
  less = pivot - 1;
  if (pivot - start < 1)
    return;
  else if (pivot - start == 1)
  {
    if (*start > *pivot)
      swap_int(start, pivot);
    return;
  }
  while (1) {
    while (start < pivot && *start <= *pivot)
      ++start;
    if (start == pivot)
      break;
    while (less > start && *less >= *pivot)
      --less;
    if (start < less)
      swap_int(start, less);
    else
    {
      swap_int(start, pivot);
      break;
    }
  }
  do_quick(cpy_start, start - 1);
  do_quick(start + 1, pivot);
}

char  *quicksort(t_array *array)
{
  int   *start;
  int   *end;
  int   middle;

  middle = array->cursor / 2;
  start = array->mem;
  end = start + (array->cursor - 1);
  do_quick(start, end);
  return ("sorted");
}
