#include "array.h"
#include "printf.h"
#include "quicksort.h"

int main()
{
  t_array *array;

  array = array_new_ints(10, 4, 6, 34, 12, 654, 1, 43, 32, 66, 67);
  array_print(array);
  quicksort(array);
  ft_printf("//////////////////\n");
  array_print(array); 
}
