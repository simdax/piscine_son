/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_program.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/11 11:07:52 by scornaz           #+#    #+#             */
/*   Updated: 2018/06/11 11:09:40 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdarg.h>
#include "opengpu.h"

t_program		create_program(char *filename, char *func_name, int n_buffers,
							   t_cl *gpu, ...)
{
	va_list		ap;
	t_program	prg;
	t_buffer	*s_buffers;
	int		i;

	if (!(init_program(&prg, &s_buffers, n_buffers, filename)))
		exit(0);
	i = -1;
	va_start(ap, gpu);
	while (++i < n_buffers)
	{
		s_buffers[i].type = va_arg(ap, t_type_buf);
		s_buffers[i].size = va_arg(ap, size_t);
		s_buffers[i].name = va_arg(ap, void*);
	}
	va_end(ap);
	create_buffers(&prg, s_buffers, n_buffers, gpu);
	if (!(prg.kernel = clCreateKernel(prg.program, func_name, &prg.err)))
		printf("\e[31mERROR !:\n\n%s\e[0m\n\n", geterrorstring(prg.err));
	i = -1;
	while (++i < n_buffers)
		clSetKernelArg(prg.kernel, i, sizeof(cl_mem), (void *)&prg.buffers[i]);
	free(s_buffers);
	return (prg);
}

t_program		create_program2(char *filename, char *func_name, int n_buffers,
								t_cl *gpu, va_list ap)
{
	t_program	prg;
	t_buffer	*s_buffers;
	int		i;

	if (!(init_program(&prg, &s_buffers, n_buffers, filename)))
		exit(0);
	i = -1;
	while (++i < n_buffers)
	{
		s_buffers[i].type = va_arg(ap, t_type_buf);
		s_buffers[i].size = va_arg(ap, size_t);
		s_buffers[i].name = va_arg(ap, void*);
	}
	create_buffers(&prg, s_buffers, n_buffers, gpu);
	if (!(prg.kernel = clCreateKernel(prg.program, func_name, &prg.err)))
		printf("\e[31mERREUR MEC !!!!:\n\n%s\e[0m\n\n", geterrorstring(prg.err));
//	char	buffer[2048];
//	size_t	l;
//	clGetPrgramBuildInfo(prg->prgram, gpu->device, CL_PRGRAM_BUILD_LOG, sizeof(buffer), buffer, &l);
//	printf("fsadjfhsda for %zd %s", l, buffer);
	i = -1;
	while (++i < n_buffers)
		clSetKernelArg(prg.kernel, i, sizeof(cl_mem), (void *)&prg.buffers[i]);
	free(s_buffers);
	return (prg);
}

t_program		create_program3(t_cl *gpu, char *filename, char *func_name,
								int n_buffers, va_list ap, void **ret)
{
	t_program	prg;
	t_buffer	*s_buffers;
	int		i;

	if (!(init_program(&prg, &s_buffers, n_buffers, filename)))
		exit(0);
	i = -1;
	while (++i < n_buffers)
	{
		s_buffers[i].type = va_arg(ap, t_type_buf);
		s_buffers[i].size = va_arg(ap, size_t);
		s_buffers[i].name = va_arg(ap, void*);
		if (s_buffers[i].type == OUTPUT)
			*ret = s_buffers[i].name;
	}
	create_buffers(&prg, s_buffers, n_buffers, gpu);
	if (!(prg.kernel = clCreateKernel(prg.program, func_name, &prg.err)))
		printf("\e[31mERROR !:\n\n%s\e[0m\n\n", geterrorstring(prg.err));
	i = -1;
	while (++i < n_buffers)
		clSetKernelArg(prg.kernel, i, sizeof(cl_mem), (void *)&prg.buffers[i]);
	free(s_buffers);
	return (prg);
}
