#include "printf.h"
#include "rng.h"

int main()
{
  int i;

  i = 10;
  while (i--)
  {
    ft_printf("%d\n", rng_range(0, 20));
  }
  i = 10;
  while (i--)
  {
    ft_printf("%d\n", rng_range(-20, 20));
  }
  i = 10;
  while (i--)
  {
    ft_printf("%d\n", rng_range(0, 1));
  }
}
