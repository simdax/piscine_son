#include "rng.h"
#include "limits.h"

int   rng()
{
  static int seed = 123456789;
  int a;
  int c;
  unsigned long long m;
  
  a = 1103515245;
  c = 12345;
  m = INT_MAX;
  seed = (a * seed + c) % m;
  return (seed);
}

float rng_float()
{
  return ((float)rng() / INT_MAX);
}

static int   ft_ceil(float val)
{
  if (val - (int)val > 0.5)
    return ((int)val + 1);
  else
    return (val);
}

int   rng_range(int min, int max)
{
  return ft_ceil((rng_float() * (max - min) + min));
}
