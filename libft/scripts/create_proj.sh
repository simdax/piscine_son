#!/bin/sh

echo new project name :
read projectname

mkdir $projectname
echo $projectname >> .modules_list

cd $projectname
cp ../generic_Makefile Makefile 
sed -i "s/projectname/$projectname/" Makefile
mkdir srcs objs bin tests lib

cat > srcs/$projectname.c << EOM
#include "$projectname.h"

char  *$projectname()
{
    return ("salut beau gosse");
}
EOM

cat > $projectname.h << EOM
char  *$projectname();
EOM

cat > tests/main.c << EOM
#include "printf.h"
#include "$projectname.h"

int main()
{
    ft_printf("%s\n", $projectname());
}
EOM

make
