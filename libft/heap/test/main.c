/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 19:47:42 by scornaz           #+#    #+#             */
/*   Updated: 2018/10/12 19:48:08 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "array.h"
#include "printf.h"
#include "heap.h"

static void	print(void *el, t_array *array)
{
	t_heap	*data;

	data = el;	
	ft_printf("%d et %d\n", *(int*)data->data, data->priority);
}

int			main()
{
	t_array		*array;
	t_heap		*heap;

	array = array_new(sizeof(t_heap), 16);
	int a = 17;
	int b = 17;
	int c = 17;
	int d = 17;
	int f = 17;
	heap_insert(array, 10, &b);
	heap_insert(array, 10, &c);
	heap_insert(array, 0, &a);
	heap_insert(array, 30, &d);
	heap_insert(array, 20, &f);
	heap_insert(array, 10, &b);
	heap_insert(array, 8, &c);
	heap_insert(array, 20, &a);
	heap_insert(array, 31, &d);
	heap_insert(array, 29, &f);
	array_for_each(array, print);
	while ((heap = heap_get(array)))
	{
		ft_printf("%d\n", heap->priority);
		free(heap);
	}
}
