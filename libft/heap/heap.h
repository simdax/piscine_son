#ifndef HEAP_H
# define HEAP_H

#include "../array/array.h"

typedef struct  s_ftheap
{
    t_array     *array;
}               t_ftheap;

typedef	struct	s_heap
{
    int		      priority;
    void	      *data;
}				        t_heap;

t_ftheap        *ft_heap_new();
t_heap		      *heap_get(t_array *array);
void		        heap_insert(t_array *array, int priority, void *val);

#endif
