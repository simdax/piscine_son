/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: scornaz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 19:47:37 by scornaz           #+#    #+#             */
/*   Updated: 2018/10/12 19:47:39 by scornaz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "heap.h"

static int	check_child(t_array *array, unsigned a, unsigned b, unsigned c)
{
  t_heap	*tmp;
  t_heap	*tmp2;
  t_heap  *tmp3;

  tmp = array_at(a, array);
  tmp2 = array_at(b, array);
  tmp3 = array_at(c, array);
  if (tmp2 && (tmp2->priority < tmp->priority) && ((!tmp3) || (tmp2->priority < tmp3->priority)))
  {
    array_swap(array, a, b);
    return (b);
  }
  else if (tmp3)
  {
    array_swap(array, a, c);
    return (c);
  }
  return (0);	
}

t_heap		*heap_get(t_array *array)
{
  int		  index;
  int		  tmp_index;
  t_heap	*tmp_ret_element;

  index = 0;
  tmp_ret_element = array_pop(array);
  if (!tmp_ret_element)
    return (0);
  if (!array->cursor)
    return (tmp_ret_element);
  ft_memswap(tmp_ret_element, array_at(0, array), array->type_len);
  while (1)
  {
    tmp_index = check_child(array, index, index * 2 + 1, index * 2 + 2);
    if (!tmp_index)
      break;
    index = tmp_index;
  }
  return (tmp_ret_element); 
}

void		heap_insert(t_array *array, int priority, void *val)
{
  int		  index_current;
  int		  index_parent;
  t_heap	*tmp_value;
  t_heap	new_value;

  index_current = array->cursor;
  new_value.priority = priority;
  new_value.data = val;
  array_add(array, &new_value, 1);
  while (index_current > 0)
  {
    index_parent = (index_current - 1) / 2;
    tmp_value = array_at(index_parent, array);
    if (tmp_value && tmp_value->priority > priority)
      array_swap(array, index_current, index_parent);
    else
      break;
    index_current = index_parent;
  }
}
