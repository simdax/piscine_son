# commands

portaudio:
	git clone https://git.assembla.com/portaudio.git portaudio
	cd portaudio && ./configure && make -j

pres:
	cd sujets/presentation && python -m http.server

bach: bin/sine_city ressources/bach.notes
	bin/sine_city ressources/bach.notes

dump: bin/midi_dump
	bin/midi_dump $(FILE)

#ressources

ressources/bach.notes: bin/midi2sine ressources/midi/bach.mid
	bin/midi2sine ressources/midi/bach.mid > ressources/bach.notes

# rules

bin/sine_city: sine_city/srcs/*.c
	mkdir -p bin
	make -C sine_city TARGET=../bin/sine_city

bin/midi2sine: midi_dump/srcs/*.c midi_dump/plugins/sine.c 
	mkdir -p bin
	make -C midi_dump sine TARGET=../bin/midi2sine

bin/midi_dump: midi_dump/srcs/*.c
	mkdir -p bin
	make -C midi_dump print TARGET=../bin/midi_dump

clean:
	rm -rf bin/*

fclean: clean
	find -name '*.a' -delete
	find -name '*.o' -delete
