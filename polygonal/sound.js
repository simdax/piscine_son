class MyWorkletNode extends AudioWorkletNode {
  constructor(context) {
      super(context, 'my-worklet-processor');
  }
}

let connected = false
function toggle()
{
	if (connected)
	{
		node.disconnect(audioContext.destination)
		connected = false
	}
	else
	{
		node.connect(audioContext.destination);
		connected = true
	}
}

let audioContext = new AudioContext();
let node;

audioContext.audioWorklet.addModule('processors.js').then(() => {
  node = new MyWorkletNode(audioContext)
});
