let painting = false;
let canvas = document.querySelector('#canvas');
let canvas_context = canvas.getContext('2d');
let centerX = canvas.width / 2;
let centerY = canvas.height / 2;

function	getImagePixel(image, x, y)
{
	let lineX = canvas.width * 4;
	let lineY = canvas.height * 4;
	return image[lineX % x + lineY / y]
}

canvas_context.beginPath();
canvas_context.arc(centerX, centerY, 30, 0, 2 * Math.PI, false);
canvas_context.lineWidth = 5;
canvas_context.strokeStyle = '#003300';
canvas_context.stroke();

let image = canvas_context.getImageData(0, 0, canvas.width, canvas.height).data

//const gpu = new GPU();
//const multiplyMatrix = gpu.createKernel(function(image, ray) {
//	var angle = 0;
//	for (var distance = 0; distance < 200; distance++) {
//		if (image)
//			sum += a[this.thread.x][]
//	}
//	return sum;
//})
//.setOutput([300])

function getPixel(x, y)
{
	return im[parseInt(x) + parseInt(y) * canvas.width]
}

let im = []
for (let i = 0; i < canvas.width * canvas.height * 4; i += 4)
{
	im.push(image[i] + image[i + 1] + image[i + 2]);
}
const precision = 300
let rays = Array(precision).fill(0)
.map((_, x) => {
	angle = (2 * Math.PI / precision) * x;
	data = 0;
	distance = 0;
	x = centerX; y = centerY;
	while (!data && distance < 100)
	{
		x = centerX + Math.cos(angle) * distance; y = centerY + Math.sin(angle) * distance;
		data = getPixel(x, y);
		distance += (1 / 20);
	}
	return (data ? [x - centerX, y - centerY] : undefined)
});
console.log(rays)
console.log(rays.findIndex(x => x > 0))
console.log(rays.filter(x => x != undefined).length)
//const c = multiplyMatrix(im, rays);

function draw (ev) {
	if (painting)
	{
		console.log(ev.x);
		console.log(ev.y);
	}
}
window.addEventListener("mouseout", function (ev){
	painting = false
})
canvas.onmousedown = function (ev) {
	console.log(ev.offsetX, ev.offsetY)
	painting = true
};
canvas.onmouseup = function () {
	painting = false
};
canvas.onmousemove = draw;

