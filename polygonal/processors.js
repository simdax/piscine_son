class MyWorkletProcessor extends AudioWorkletProcessor {
	static get parameterDescriptors() {
		return [{
			name: 'freq',
			defaultValue: 400
		}];
	}
	constructor() {
		super();
		this.time = 0
	}

	process(inputs, outputs, parameters) {
		const out = outputs[0];
		const in_ = inputs[0];
		const freq = parameters.freq;
		for (let i = 0; i < out[0].length; i++)
		{
			out[0][i] = Math.sin(2 * Math.PI * freq * this.time); 
			this.time += 1 / 41000;
		}
		return true
	}
}

registerProcessor('my-worklet-processor', MyWorkletProcessor);
