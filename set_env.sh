#!/bin/sh

ROOT=`pwd`

export LD_LIBRARY_PATH=$ROOT/portaudio/lib/.libs
export LIBRARY_PATH=$ROOT/libft
export CPATH=$ROOT/libft:$ROOT/portaudio
