#!/bin/sh

realpath() {
	    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
	}

ROOT=`realpath ..`

export CPATH=$ROOT/libft:$ROOT/portaudio
export LIBRARY_PATH=$ROOT/libft:$ROOT/portaudio/lib
