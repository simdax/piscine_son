#include <stdio.h>
#include "libft.h"
#include "ugen.h"
#include "audio_context.h"


void sheppard_tones(t_ugen *out, int n)
{
  t_ugen *u = ugen_new(2000.0 / n, 0, 0, 0);
  t_env freq_env = env_new(20000.0 / n, 1, 16.0, 0.0);
  t_env amp_env = env_new(0.03, 2, 8.0, 0.1, 8.0, 0.0);
  ugen_set_env(&u->freq, &freq_env);
  ugen_set_env(&u->amp, &amp_env);
  ugen_set_ugen(&out->add, u);
  if (n > 1)
    return (sheppard_tones(u, n - 1));
}

int main()
{
  int i = 0;
  t_audio_context audio_context;

  audio_context = new_audio_context();
  sheppard_tones(audio_context.out, 16);
  audio_context.start(&audio_context);
  while (1)
  {
    sleep(3); 
    sheppard_tones(ugen_get_last(audio_context.out), 16);
  }
  audio_context.close(&audio_context); 
}
