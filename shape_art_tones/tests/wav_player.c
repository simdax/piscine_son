#include "audio_context.h"
#include "ugen.h"
#include "wav.h"

int main(int argc, char **argv)
{
	t_wav		        *wav_file;
	t_audio_context	ctx;

	ctx = new_audio_context();
	wav_file = wav_new(argv[1]);
  if (!wav_file)
  {
    write(2, "no wave file\n", 12); 
    return (1);
  }
  double  function(double io)
  {
    static int i = 0;
    return (wav_file->channels[0][i++]);
  }
	ctx.out->func = function;
  ctx.start(&ctx); 
  Pa_Sleep(20 * 1000);
  ctx.close(&ctx); 
}
