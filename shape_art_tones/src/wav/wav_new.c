#include "wav.h"
#include "printf.h"

t_wav	*wav_new(char *path)
{
  t_wav	*wav_file;

  if (!path)
    return (0);
  ft_printf("opening : %s\n", path);
  wav_file = malloc(sizeof(*wav_file));
  // DO THE CHECK
  if (!(wav_parse(wav_file, path, 1)))
  {
    free(wav_file);
    return (0);
  }
  return (wav_file);
}
