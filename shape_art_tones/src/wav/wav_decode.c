#include <unistd.h>
#include "portaudio.h"
#include "wav.h"
#include "libft.h"

static int	skip_until_data(int fd, int gate)
{
  // TODO: parse that stuff
  char              	data[5];

  read(fd, &data, 3);
  while (read(fd, &data[3], 1))
  {
    if (ft_strequ(data, "data")){
      gate = 1;
      break;
    }
    else
      ft_memmove(&data[0], &data[1], 4);
  }
  return (gate);
}

void	wav_get_data(t_wav *wav, int frames, float* buffer)
{
  int 	i;
  short	tmp_s;
  short	nb_bytes;

  nb_bytes = wav->header.bits_per_sample / 8;
  i = 0;
  while (i < frames && wav->head < wav->header.data_length)
  {
    ft_memcpy(&tmp_s, wav->pcm + wav->head, nb_bytes);
    buffer[i] = (float)tmp_s / 32768;
    ++i;
    wav->head += nb_bytes;
  }
}

static void populate_channels(t_wav *wav)
{
  int i;
  int j;

  j = 0;
  while (j < wav->header.nb_channels)
  {
    wav->channels[j] = (float*)malloc(wav->header.data_length);
    ++j; 
  }
  i = 0;
  while (i < wav->header.data_length / wav->header.block_size)
  {
    j = 0;
    while (j < wav->header.nb_channels)
    {
      wav->channels[j][i] = wav->float_data[i * wav->header.nb_channels + j];
      ++j; 
    }
    ++i;
  }
}

int		wav_parse(t_wav *wav, char *path, int copy)
{
  int      	fd;
  int			  gate;

  fd = open(path, O_RDONLY);
  if (!fd)
    return (0);
  read(fd, &wav->header, sizeof(t_wav_file_header));
  if ((gate = skip_until_data(fd, 0)))
  {
    read(fd, &wav->header.data_length, 4);
    wav->pcm = malloc(wav->header.data_length);
    wav->float_data = malloc(wav->header.data_length);
    wav->channels = malloc(wav->header.nb_channels * sizeof(float*));
    read(fd, wav->pcm, wav->header.data_length);
    wav_get_data(wav, wav->header.data_length, wav->float_data);
    populate_channels(wav);
    wav->head = 0;
    wav->fd = fd;
    return (1);
  }
  else
    return (0);
}
