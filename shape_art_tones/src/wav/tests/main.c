#include "portaudio.h"
#include "libft.h"
#include "printf/includes/printf.h"
#include "../wave.h"

static int audio_callback(
    const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer,
    const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags,
    void *userData)
{
  wav_get_data(userData, framesPerBuffer * 2, outputBuffer);
  return (paContinue);
}

int      main(int argc, char **argv)
{
  t_wav             wav;
  PaStream	*stream;

  wav_parse(&wav, argv[1], 1);
  Pa_Initialize();
  Pa_OpenDefaultStream(&stream, 0, wav.header.nb_channels,
      paFloat32, wav.header.sample_rate,
      BUF_SIZE, audio_callback, &wav);
  Pa_StartStream(stream);
  Pa_Sleep(20 * 1000);
//  //window(&app);
  Pa_Terminate();
  close(wav.fd);
  return (0);
}
