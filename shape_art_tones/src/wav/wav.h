#ifndef WAVE_H
# define WAVE_H

# include "libft.h"
# define BUF_SIZE  512

typedef struct  s_wav_file_header
{
  char            file_type_block_id[4]; 
  unsigned        length;
  char            file_format_id[4]; 
  char            format_block_id[4]; 
  unsigned        block_size;
  unsigned short  audio_format;
  unsigned short  nb_channels;
  unsigned        sample_rate;
  unsigned        byte_per_sec;
  unsigned short  byte_per_block;
  unsigned short  bits_per_sample;
  unsigned        data_length;
}               t_wav_file_header;

typedef struct  s_wav
{
  t_wav_file_header header;
  int             	fd;
  char		          *pcm;
  float		          *float_data;
  float		          **channels;
  size_t	          head;
}               t_wav;

t_wav	*wav_new(char *path);
int	wav_parse(t_wav *app, char *path, int copy);
int	wav_read_data(t_wav *app, int frames, float* buffer);
void	wav_get_data(t_wav *app, int frames, float* buffer);

#endif
