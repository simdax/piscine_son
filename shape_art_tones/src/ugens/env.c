#include "ugen.h"

t_env   env_new(double start, unsigned nb, ...)
{
  t_env     env;
  unsigned  i;
  va_list   list;
  unsigned  dur;
  unsigned  dur_tmp;
  double    value_tmp;
  double    slope;

  env.gate = 1;
  env.value = start;
  env.omega = 0;
  env.pts = 0;
  i = 0;
  dur = 0;
  va_start(list, nb);
  while (i++ < nb)
  {
    dur_tmp = SEC(va_arg(list, double));
    value_tmp = va_arg(list, double);
    slope =  (value_tmp - start) / dur_tmp;
    start = value_tmp;
    dur += dur_tmp;
    ft_lstaddlast(&env.pts, ft_lstnew(&(t_env_node){slope, dur}, sizeof(t_env_node)));
  }
  va_end(list);
  return (env);
}

double  env_eval(t_env *env)
{
  t_env_node *node;

  if (!env->pts || !env->pts->content)
    return (0);
  node = env->pts->content;
  if (node->time > 0)
    ++env->omega;
  env->value += node->slope;
  if (env->omega > node->time)
    env->pts = env->pts->next;
  return (env->value);
}
