#ifndef UGEN_H
# define UGEN_H

# include <math.h>
# include <stdarg.h>
# include "portaudio.h"
# include "libft.h"

# define SEC(x) (SAMPLE_RATE * x)
# define PHASE(F) (2 * M_PI / SAMPLE_RATE) * F

typedef double  (*f_ugen)(double);

typedef enum    e_arg_type
{
  SCALAR,
  ENV,
  UGEN
}               t_arg_type;

typedef union   u_arg 
{
  double          scalar;
  struct  s_ugen  *ugen;
  struct  s_env   *env;
}               t_uarg;

typedef struct  s_arg
{
  t_arg_type type;
  t_uarg u_arg;
}               t_arg; 

typedef struct  s_ugen
{
  double p_omega;
  t_arg  freq;
  t_arg  phase;
  t_arg  amp;
  t_arg  add;
  f_ugen  func;
}               t_ugen;

typedef struct  s_env_node
{
  double    slope;
  unsigned  time;
}               t_env_node;

typedef struct  s_env
{
  double  p_omega;
  double  omega;
  double  value;
  t_list  *pts;
  int      gate;
}               t_env;

t_ugen  *ugen_new(double freq, double phase, double amplitude, double add);
void    ugen_free(t_ugen *ugen);
double  ugen_eval(t_ugen*);
t_ugen  *ugen_get_last(t_ugen *ugen);

t_env   env_new(double start, unsigned nb, ...);
double  env_eval(t_env *env);

void    ugen_set_scalar(t_arg *arg, double scalar);
void    ugen_set_env(t_arg *arg, t_env *env);
void    ugen_set_ugen(t_arg *arg, t_ugen *ugen);

double	ugen_sin(double phase);

#endif
