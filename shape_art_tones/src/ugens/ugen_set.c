#include "ugen.h"
#include "libft.h"

void  ugen_set_scalar(t_arg *arg, double scalar)
{
  if (arg->type == UGEN)
    free(arg->u_arg.ugen);
  else if (arg->type == ENV)
    free(arg->u_arg.env);
  arg->u_arg.scalar = scalar;
  arg->type = SCALAR;
}

void  ugen_set_ugen(t_arg *arg, t_ugen *ugen)
{
  if (arg->type == ENV)
    free(arg->u_arg.env);
  arg->u_arg.ugen = ugen;
  arg->type = UGEN;
}

void  ugen_set_env(t_arg *arg, t_env *env)
{
  if (arg->type == UGEN)
    free(arg->u_arg.ugen);
  if (arg->type == SCALAR || arg->type == UGEN)
    arg->u_arg.env = malloc(sizeof(t_env));
  ft_memcpy(arg->u_arg.env, env, sizeof(t_env));
  arg->type = ENV;
}
