#include <math.h>
#include <stdarg.h>
#include "ugen.h"

t_ugen  *ugen_get_last(t_ugen *ugen)
{
  if (ugen->add.type == UGEN)
    return (ugen_get_last(ugen->add.u_arg.ugen));
  return (ugen);
}

t_ugen  *ugen_new(double freq, double phase, double amplitude, double add)
{
  t_ugen *ugen;

  ugen = malloc(sizeof(*ugen));
  *ugen = (t_ugen){
    0, {SCALAR, freq}, {SCALAR, phase}, {SCALAR, amplitude}, {SCALAR, add}, ugen_sin
  };
  return (ugen);
}

void    ugen_free(t_ugen *ugen)
{
  if (ugen->freq.type == UGEN) 
    ugen_free(ugen->freq.u_arg.ugen);
  if (ugen->phase.type == UGEN) 
    ugen_free(ugen->phase.u_arg.ugen);
  if (ugen->amp.type == UGEN) 
    ugen_free(ugen->amp.u_arg.ugen);
  if (ugen->add.type == UGEN) 
    ugen_free(ugen->add.u_arg.ugen);
}

double  get_arg(t_arg arg)
{
  if (arg.type == SCALAR) 
    return (arg.u_arg.scalar);
  else if (arg.type == UGEN) 
    return (ugen_eval(arg.u_arg.ugen));
  else if (arg.type == ENV) 
    return (env_eval(arg.u_arg.env));
  else
    return (0);
}

double  ugen_eval(t_ugen *ugen)
{
  double freq;
  double phase;
  double amplitude;
  double result;
  double add;
  //double a = M_PI / 30;
  //double b = 2 * a;

  add = get_arg(ugen->add); 
  phase = get_arg(ugen->phase); 
  amplitude = get_arg(ugen->amp); 
  freq = PHASE(get_arg(ugen->freq)); 
  result = ugen->func(ugen->p_omega + phase) * amplitude + add; //* (cos(a) / cos(b * (theta/b - abs(theta/b)) - a));
  ugen->p_omega += freq;
  return (result);
}
