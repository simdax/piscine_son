#include "audio_context.h"

static void  *play(void *c) 
{
  unsigned  i;
  unsigned  j;
  t_audio_context *ctx;
  float     audio_buffer[512];

  ctx = c;
  Pa_StartStream(ctx->stream);
  j = 0;
  while (ctx->time < 0 || j <= ctx->time)
  {
    i = 0;
    while (i < 512)
      audio_buffer[i++] = (float)ugen_eval(ctx->out);
    Pa_WriteStream(ctx->stream, audio_buffer, sizeof(audio_buffer) / sizeof(audio_buffer[0]));
    j += i;
  }
  Pa_StopStream(ctx->stream);
  return (0);
}

static void  start(t_audio_context *ctx)
{
  pthread_create(&ctx->main_thread, 0, play, ctx);
}

static void  close_audio_context(t_audio_context *ctx)
{
  Pa_Terminate();
  ugen_free(ctx->out);
}

t_audio_context new_audio_context()
{
  t_audio_context ctx;

  Pa_Initialize();
  Pa_OpenDefaultStream(
	  &ctx.stream, 0, 1,
	  paFloat32, SAMPLE_RATE, 512, 
    0, 0
  );
  ctx.start = start; 
  ctx.close = close_audio_context; 
  ctx.time = -1; 
  ctx.out = ugen_new(0, 0, 0.2, 0);
  return (ctx);
}
