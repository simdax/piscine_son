#ifndef AUDIO_CONTEXT_H
# define AUDIO_CONTEXT_H

#include "portaudio.h"
#include "ugen.h"
#include <pthread.h>

typedef struct  s_audio_context
{
  PaStream  *stream;
  t_ugen    *out;
  long      time;
  void      (*init)();
  void      (*start)(struct s_audio_context *ctx);
  void      (*close)(struct s_audio_context *ctx);
  pthread_t main_thread;
}               t_audio_context;

//TODO: something like a singleton ?
t_audio_context new_audio_context();

#endif
