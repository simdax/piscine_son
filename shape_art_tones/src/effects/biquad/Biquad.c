#include "Biquad.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void bq_load_coefficients(t_biquad *bq, int filter_type,
    float A, float omega,
    float sn, float cs,
    float alpha, float beta)
{
  switch (filter_type){
    case LOWPASS:
      bq->b0 = (1.0 - cs) / 2.0;
      bq->b1 = 1.0 - cs;
      bq->b2 = (1.0 - cs) / 2.0;
      bq->a0 = 1.0 + alpha;
      bq->a1 = -2.0 * cs;
      bq->a2 = 1.0 - alpha;
      break;
    case HIGHPASS:
      bq->b0 = (1 + cs) / 2.0;
      bq->b1 = -(1 + cs);
      bq->b2 = (1 + cs) / 2.0;
      bq->a0 = 1 + alpha;
      bq->a1 = -2 * cs;
      bq->a2 = 1 - alpha;
      break;
    case BANDPASS:
      bq->b0 = alpha;
      bq->b1 = 0;
      bq->b2 = -alpha;
      bq->a0 = 1 + alpha;
      bq->a1 = -2 * cs;
      bq->a2 = 1 - alpha;
      break;
    case NOTCH:
      bq->b0 = 1;
      bq->b1 = -2 * cs;
      bq->b2 = 1;
      bq->a0 = 1 + alpha;
      bq->a1 = -2 * cs;
      bq->a2 = 1 - alpha;
      break;
    case PEAK:
      bq->b0 = 1 + (alpha * A);
      bq->b1 = -2 * cs;
      bq->b2 = 1 - (alpha * A);
      bq->a0 = 1 + (alpha / A);
      bq->a1 = -2 * cs;
      bq->a2 = 1 - (alpha / A);
      break;
    case LOWSHELF:
      bq->b0 = A * ((A + 1) - (A - 1) * cs + beta * sn);
      bq->b1 = 2 * A * ((A - 1) - (A + 1) * cs);
      bq->b2 = A * ((A + 1) - (A - 1) * cs - beta * sn);
      bq->a0 = (A + 1) + (A - 1) * cs + beta * sn;
      bq->a1 = -2 * ((A - 1) + (A + 1) * cs);
      bq->a2 = (A + 1) + (A - 1) * cs - beta * sn;
      break;
    case HIGHSHELF:
      bq->b0 = A * ((A + 1) + (A - 1) * cs + beta * sn);
      bq->b1 = -2 * A * ((A - 1) + (A + 1) * cs);
      bq->b2 = A * ((A + 1) + (A - 1) * cs - beta * sn);
      bq->a0 = (A + 1) - (A - 1) * cs + beta * sn;
      bq->a1 = 2 * ((A - 1) - (A + 1) * cs);
      bq->a2 = (A + 1) - (A - 1) * cs - beta * sn;
      break;
    default:
      break;
  }
}

void    bq_print_info(t_biquad* bq)
{
  printf("A0: %.8f\n",bq->a0);
  printf("A1: %.8f\n",bq->a1);
  printf("A2: %.8f\n",bq->a2);
  printf("B0: %.8f\n",bq->b0);
  printf("B1: %.8f\n",bq->b1);
  printf("B2: %.8f\n",bq->b2);
  printf("Last input: %.8f\n",bq->prev_input_1);
  printf("Second to last input: %.8f\n",bq->prev_input_2);
  printf("Last output: %.8f\n",bq->prev_output_1);
  printf("Second to last output: %.8f\n",bq->prev_output_2);
}

void      filter_update(t_filter *filter)
{
  float A = pow(10, filter->db_gain / 40);
  float omega = 2 * M_PI * filter->frequency / filter->sample_rate;
  float sn = sin(omega);
  float cs = cos(omega);
  float alpha = sn / (2 * filter->q);
  float beta = sqrt(A + A);
  
  bq_load_coefficients(&(filter->biquad), filter->filter_type,
      A, omega, sn, cs, alpha, beta);
  filter->biquad.a1 /= filter->biquad.a0;
  filter->biquad.a2 /= filter->biquad.a0;
  filter->biquad.b0 /= filter->biquad.a0;
  filter->biquad.b1 /= filter->biquad.a0;
  filter->biquad.b2 /= filter->biquad.a0;
//  filter->biquad.prev_input_1 = 0.0;
//  filter->biquad.prev_input_2 = 0.0;
//  filter->biquad.prev_output_1 = 0.0;
//  filter->biquad.prev_output_2 = 0.0;
}

float bq_process(t_biquad *bq, float input)
{
  float output;
  
  output = (bq->b0 * input) +
    (bq->b1 * bq->prev_input_1) +
    (bq->b2 * bq->prev_input_2) -
    (bq->a1 * bq->prev_output_1) -
    (bq->a2 * bq->prev_output_2);
  bq->prev_input_2 = bq->prev_input_1;
  bq->prev_input_1 = input;
  bq->prev_output_2 = bq->prev_output_1;
  bq->prev_output_1 = output;
  return (output);
}
