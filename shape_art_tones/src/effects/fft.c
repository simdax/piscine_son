#include <math.h>
#include <complex.h>
#include <stdio.h>

void    fft(float* dest, double* X, int N);

void separate (double* a, int n) {
    double b[n / 2];  
    for(int i = 0; i < n / 2; i++)   
        b[i] = a[i * 2 + 1];
    for(int i = 0; i < n / 2; i++)  
        a[i] = a[i * 2];
    for(int i = 0; i < n / 2; i++) 
        a[i + n / 2] = b[i];
}

#define clear() printf("\033[H\033[J")

void fft (float *dst, double *X, int N) 
{
    if(N < 2) {
    } else {
        separate(X, N);      
        fft(dst, X, N / 2); 
        fft(dst, X + N / 2, N / 2); 
        for(int k = 0; k < N / 2; k++) {
            double e = X[k];   
            double o = X[k + N / 2];
            double w = exp(0 + (-2. * M_PI * k / N) * I);
            dst[k] = e + w * o;
            dst[k + N / 2] = e - w * o;
        }
    }
}
