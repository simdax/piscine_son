#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "linmath.h"
#include "Biquad.h"
#include "wave.h"
#include <stdlib.h>
#include <stdio.h>

static const struct
{
  float x, y;
  float r, g, b;
} vertices[6] =
{
  { -1.f, 1.f, 1.f, 0.f, 0.f },
  { 1.f, 1.f, 0.f, 1.f, 0.f },
  { 1.f,  -1.f, 0.f, 0.f, 1.f },
  { -1.f, 1.f, 1.f, 0.f, 0.f },
  { -1.f, -1.f, 0.f, 1.f, 0.f },
  { 1.f,  -1.f, 0.f, 0.f, 1.f },
};

static const char* vertex_shader_text =
"#version 110\n"
"uniform mat4 MVP;\n"
"attribute vec3 vCol;\n"
"attribute vec2 vPos;\n"
"varying vec3 color;\n"
"void main()\n"
"{\n"
"    gl_Position = MVP * vec4(vPos, 0.0, 1.0);\n"
"    color = vec3(1, gl_Position.y, 0);\n"
"}\n";

static const char* fragment_shader_text =
"#version 110\n"
"varying vec3 color;\n"
"uniform float waveData[512];"
"void main()\n"
"{\n"
"    int index = int(gl_FragCoord.x) / 2;"
"    float val = waveData[index];" 
"    gl_FragColor = vec4(0.4, abs(val) > gl_FragCoord.y / 512., 0, 1.0);\n"
"}\n";

static void error_callback(int error, const char* description)
{
  fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  t_filter  *filter;

  filter = (t_filter*)glfwGetWindowUserPointer(window);
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
  {
    glfwSetWindowShouldClose(window, GLFW_TRUE);
    return ;
  }
  if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
    filter->filter_type = (FILTER_TYPES)(filter->filter_type + 1) % 7;
  else if (key == GLFW_KEY_K && action == GLFW_REPEAT)
    filter->db_gain += 1;
  else if (key == GLFW_KEY_J && action == GLFW_REPEAT)
    filter->db_gain -= 1;
  else if (key == GLFW_KEY_S && action == GLFW_REPEAT)
    filter->frequency += 100;
  else if (key == GLFW_KEY_D && action == GLFW_REPEAT)
  {
     if (filter->frequency <= 10)
      filter->frequency = 10;
     else
       filter->frequency -= 100;
  }
  filter_update(filter);
}

int window(t_app *app)
{
  GLFWwindow* window;
  GLuint vertex_buffer, vertex_shader, fragment_shader, program;
  GLint mvp_location, vpos_location, vcol_location;

  glfwSetErrorCallback(error_callback);
  if (!glfwInit())
    exit(EXIT_FAILURE);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  window = glfwCreateWindow(512, 512, "Simple example", NULL, NULL);
  if (!window)
  {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }
  glfwSetKeyCallback(window, key_callback);
  glfwSetWindowUserPointer(window, &app->filter);
  glfwMakeContextCurrent(window);
  gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
  glfwSwapInterval(1);
  // NOTE: OpenGL error checks have been omitted for brevity
  glGenBuffers(1, &vertex_buffer);
  glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex_shader, 1, &vertex_shader_text, NULL);
  glCompileShader(vertex_shader);
  fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment_shader, 1, &fragment_shader_text, NULL);
  glCompileShader(fragment_shader);
  GLint isCompiled = 0;
  glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &isCompiled);
  if (isCompiled == GL_FALSE)
  {
    GLint maxLength = 0;
    glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &maxLength);
    GLchar errorLog[maxLength];
    glGetShaderInfoLog(fragment_shader, maxLength, &maxLength, &errorLog[0]);
    printf("ERROR for fragment shader: %s", errorLog);
    glDeleteShader(fragment_shader);
    exit(1);
  }
  program = glCreateProgram();
  glAttachShader(program, vertex_shader);
  glAttachShader(program, fragment_shader);
  glLinkProgram(program);
  mvp_location = glGetUniformLocation(program, "MVP");
  vpos_location = glGetAttribLocation(program, "vPos");
  vcol_location = glGetAttribLocation(program, "vCol");
  glEnableVertexAttribArray(vpos_location);
  glVertexAttribPointer(vpos_location, 2, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), (void*) 0);
  glEnableVertexAttribArray(vcol_location);
  glVertexAttribPointer(vcol_location, 3, GL_FLOAT, GL_FALSE, sizeof(vertices[0]), (void*) (sizeof(float) * 2));
  GLint dataLoc = glGetUniformLocation(program, "waveData");
  while (!glfwWindowShouldClose(window))
  {
    glUniform1fv(dataLoc, 512, app->fft_buffer);
    float ratio;
    int width, height;
    mat4x4 m, p, mvp;
    glfwGetFramebufferSize(window, &width, &height);
    ratio = width / (float) height;
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT);
    mat4x4_identity(m);
    //mat4x4_rotate_Z(m, m, (float) glfwGetTime());
    mat4x4_ortho(p, -ratio, ratio, -1.f, 1.f, 1.f, -1.f);
    mat4x4_mul(mvp, p, m);
    glUseProgram(program);
    glUniformMatrix4fv(mvp_location, 1, GL_FALSE, (const GLfloat*) mvp);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glfwSwapBuffers(window);
    glfwPollEvents();
  }
  glfwDestroyWindow(window);
  glfwTerminate();
  exit(EXIT_SUCCESS);
}
