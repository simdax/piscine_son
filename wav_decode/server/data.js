
export default (src, audio) => {
  const dest = audio.createBuffer(src.numberOfChannels, src.length * 2, 44100);
  let chan = 0;
  while (chan < src.numberOfChannels)
  {
    const srcData = src.getChannelData(chan);
    const destData = dest.getChannelData(chan);
    for (let i in srcData)
    {
      destData[i * 2] = srcData[i];
      destData[i * 2 + 1] = destData[i * 2] + (srcData[i + 1] - srcData[i]) / 2;
    }
    ++chan;
  }
  return (dest);
}
