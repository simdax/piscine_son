registerProcessor('processor', 
  class Processor extends AudioWorkletProcessor{

    static get parameterDescriptors(){
      return [
        {
          name: 'freq', defaultValue: 1.0
        },
        {
          name: 'overlap', defaultValue: 0.5
        },
        {
          name: 'grainSize', defaultValue: 128
        },
      ]
    }
    hannWindow(length) {
      const w = new Float32Array(length);
      for (let i = 0; i < length; i++) {
        w[i] = 0.5 * (1 - Math.cos(2 * Math.PI * i / (length - 1)));
      }
      return w;
    }
    linearInterpolation(a, b, t) {
      return a + (b - a) * t;
    } 
    newGrainSize(grainSize){
      this.buffer = new Float32Array(grainSize * 2);
      this.grainWindow = this.hannWindow(grainSize);
    }

    constructor(){
      super();
      this.newGrainSize(128);
      this.i = 0;
      const routine = this.processCoRoutine();
      routine.next();
      this.getData = (arg) => routine.next(arg);
      this.grain = false;
      this.port.onmessage = (msg) => {
        const data = msg.data;
        switch(data.key) {
          case 'grain':
            this.newGrainSize(data.data);
            break;
          case 'data':
            this.data = data.data[0];
            break;
          case 'restart':
            this.i = 0
            break;
          default:
            console.log("unknow message");
        }
      }
    }
    grainSynthesis(inputData, pitchRatio, grainSize, overlapRatio){
      const grainData = new Float32Array(grainSize * 2);

      for (let i = 0; i < inputData.length; i++) {
        inputData[i] *= this.grainWindow[i];
        this.buffer[i] = this.buffer[i + grainSize];
        this.buffer[i + grainSize] = 0.0;
      }
      for (let i = 0, j = 0.0; i < grainSize; i++, j += pitchRatio ) {
        const index = Math.floor(j) % grainSize;
        const a = inputData[index];
        const b = inputData[(index + 1) % grainSize];
        grainData[i] += this.linearInterpolation(a, b, j % 1.0) * this.grainWindow[i];
      }
      for (let i = 0; i < grainSize; i += Math.round(grainSize * (1 - overlapRatio))) {
        for (let j = 0; j <= grainSize; j++) {
          this.buffer[i + j] += grainData[j];
        }
      }
    }
    *processCoRoutine(params){
      // don't bother about that. Just JS quirck
      params = yield 0; 
      while (1) {
        const grainSize = params.grainSize[0];
        const overlapRatio = params.overlap[0];
        const pitchRatio = params.freq[0];
        const inputData = this.data.slice(this.i, this.i + grainSize);

        this.i += grainSize;   
        this.grainSynthesis(inputData, pitchRatio, grainSize, overlapRatio);
        for (let i = 0; i < this.buffer.length / 128 ; i += 128 )
          params = yield this.buffer.slice(i, i + 128);
      }
    }
    process(ins, outs, params)
    {
      const data = this.getData(params).value;
      for (let i = 0 ; i < data.length; ++i)
        outs[0][0][i] = data[i];
      return true;
    }
  }
)
