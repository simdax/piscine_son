import audioLoader from 'audio-loader';
import Vue from 'vue';
import convertData from './data';
import VueAudio from './vue-audio-visual'

Vue.use(VueAudio);

const audio = new AudioContext();

const template =`
  <div>
  <av-line ref='io'/>
  <button @click='startSound'> started : {{this.started}} </button>
  <button @click='node.port.postMessage({key: "restart"})'>  restart </button>
  grain ? <input type='checkbox' 
  @input="node.port.postMessage({key:'grain'})">
  </br>
  <input v-model='freq' type=range min='0.1' max='2' value=1 step='0.01' 
  @input='set("freq", freq)'> transpose {{freq}} </button>
  </br>
  <input type=range min='0.01' max='0.75' value=1 step='0.01' v-model="overlap"
  @input='set("overlap", overlap)'> overlap {{overlap}} </button>
  </br>
  <input type=range min='7' max='10' value=8 step='1' v-model="size" 
  @input='node.port.postMessage({key: "grain", data: 2 ** size})'> grainSize : 2 ^ {{size}}</button>
  </div>
  `

new Vue({
  el: '#app', 
  template,
  data: {
    started: false,
    overlap: 0.5, 
    freq: 1, 
    size: 7, 
  },
  mounted() {
    const wave = this.$refs.io;
    audio.audioWorklet.addModule('audio.js').then(() => {
      audioLoader('good/static/audio/anorak.wav').then(buffer => {
        this.node = new AudioWorkletNode(audio, 'processor');
        this.analyser = audio.createAnalyser();
        wave.analyser = this.analyser;
        this.analyser.connect(audio.destination);
        this.node.port.postMessage({ 
          key: 'data',
          data: new Array(buffer.numberOfChannels).fill(0)
          .map((_, i) => {
            return buffer.getChannelData(i)
          })
        }) 
      })
    })
  },
  methods: {
    set(key, value){
      this.node.parameters.get(key).value = value
    },
    startSound(){
      if (this.started)
        this.node.disconnect(this.analyser)
      else
        this.node.connect(this.analyser)
      this.started = !this.started;
    }
  }
})

