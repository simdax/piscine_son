module.exports = {
  entry: './main.js',
  output: {
    filename: 'bundle.js'
  },
  resolve:{
    alias: {
      vue: 'vue/dist/vue.js'
    }
  }
}
