#ifndef WAVE_H
# define WAVE_H

# include "Biquad.h"

# define BUF_SIZE  512

typedef struct  s_wav_file_header
{
  char            file_type_block_id[4]; 
  unsigned        length;
  char            file_format_id[4]; 
  char            format_block_id[4]; 
  unsigned        block_size;
  unsigned short  audio_format;
  unsigned short  nb_channels;
  unsigned        sample_rate;
  unsigned        byte_per_sec;
  unsigned short  byte_per_block;
  unsigned short  bits_per_sample;
  unsigned        data_length;
}               t_wav_file_header;

typedef struct  s_app
{
  t_wav_file_header  header;
  int             fd;
  float           fft_buffer[BUF_SIZE];
  t_filter        filter;
}               t_app;

void    decode_audio(t_app *app);
void    fft(float* dest, double* X, int N);
int     window(t_app *app);

#endif
