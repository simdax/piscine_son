#ifndef BIQUAD_H
# define BIQUAD_H

typedef enum e_ft {
	LOWPASS 	,
	HIGHPASS 	,
	BANDPASS 	,
	NOTCH		  ,
	PEAK		  ,
	LOWSHELF	,
	HIGHSHELF	,
} FILTER_TYPES;

typedef struct s_biquad{
	float   a0;
	float   a1;
	float   a2;
	float   b0;
	float   b1;
	float   b2;
	float   prev_input_1;
	float   prev_input_2;
	float   prev_output_1;
	float   prev_output_2;
}             t_biquad;

typedef struct  s_filter
{
  t_biquad      biquad;
  FILTER_TYPES  filter_type;
  float         frequency;
  float         q;
  float         db_gain;
  int           sample_rate;
}               t_filter;

void    filter_update(t_filter *filter);
float   bq_process(t_biquad *bq, float input);
void    bq_print_info(t_biquad* bq);
  
#endif
