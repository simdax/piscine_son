#include "portaudio.h"
#include "libft.h"
#include "printf/includes/printf.h"
#include "wave.h"

void      parse_wav(t_app *app, char *path)
{
  int               fd;
  t_wav_file_header header;
  char              data[5];

  fd = open(path, O_RDONLY);
  if (!fd)
    exit(1);
  read(fd, &header, sizeof(t_wav_file_header));
  read(fd, &data, 3);
  while (read(fd, &data[3], 1))
  {
    if (ft_strequ(data, "data"))
      break;
    else
      ft_memmove(&data[0], &data[1], 4);
  }
  ft_printf("found : %s chunk\n", data);
  read(fd, &header.data_length, 4);
  app->header = header;
  app->fd = fd;
  app->filter.filter_type = LOWPASS;
  app->filter.frequency = 300;
  app->filter.q = 0.5;
  app->filter.db_gain = 2;
  app->filter.sample_rate = header.sample_rate;
  filter_update(&app->filter);
}

int      main(int argc, char **argv)
{
  t_app             app;

  parse_wav(&app, argv[1]);
  Pa_Initialize();
  decode_audio(&app);
  window(&app);
  Pa_Terminate();
  close(app.fd);
  return (0);
}
