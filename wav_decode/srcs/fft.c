#include <math.h>
#include <complex.h>
#include <stdio.h>

#define M_PI 3.14159265358979323846 // Pi constant with double precision

// separate even/odd elements to lower/upper halves of array respectively.
// Due to Butterfly combinations, this turns out to be the simplest way 
// to get the job done without clobbering the wrong elements.
void separate (double* a, int n) {
    double b[n / 2];  // get temp heap storage
    for(int i = 0; i < n / 2; i++)    // copy all odd elements to heap storage
        b[i] = a[i * 2 + 1];
    for(int i = 0; i < n / 2; i++)    // copy all even elements to lower-half of a[]
        a[i] = a[i * 2];
    for(int i = 0; i < n / 2; i++)    // copy all odd (from heap) to upper-half of a[]
        a[i + n / 2] = b[i];
}

// N must be a power-of-2, or bad things will happen.
// Currently no check for this condition.
//
// N input samples in X[] are FFT'd and results left in X[].
// Because of Nyquist theorem, N samples means 
// only first N/2 FFT results in X[] are the answer.
// (upper half of X[] is a reflection with no new information).

#define clear() printf("\033[H\033[J")

void fft (float *dst, double *X, int N) 
{
    if(N < 2) {
        // bottom of recursion.
        // Do nothing here, because already X[0] = x[0]
    } else {
        separate(X, N);      // all evens to lower half, all odds to upper half
        fft(dst, X, N / 2);   // recurse even items
        fft(dst, X + N / 2, N / 2);   // recurse odd  items
        // combine results of two half recursions
        //clear();
        for(int k = 0; k < N / 2; k++) {
            double e = X[k];   // even
            double o = X[k + N / 2];   // odd
                         // w is the "twiddle-factor"
            double w = exp(0 + (-2. * M_PI * k / N) * I);
            dst[k] = e + w * o;
            dst[k + N / 2] = e - w * o;
            //printf("%f ", dst[k]);
        }
    }
}
