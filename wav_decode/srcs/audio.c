#include <unistd.h>
#include "portaudio.h"
#include "wave.h"
#include "libft.h"

static int audio_callback(
    const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer,
    const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags,
    void *userData)
{
  t_app         *app = (t_app*)userData;
  float         *out = (float*)outputBuffer;
  unsigned      size = framesPerBuffer * app->header.byte_per_block;
  unsigned      size2 = size / app->header.nb_channels;
  unsigned char data[size];
  unsigned char *data_ptr = &data[0];
  float         tmp;
  double        outs[2][512];
  unsigned      i;
  short         tmp_s;

  read(app->fd, data_ptr, size);
  i = 0;
printf("%d %d %zu\n", size, size2, framesPerBuffer);
  while (size2--)
  {
    ft_memcpy(&tmp_s, data_ptr, app->header.bits_per_sample / 8);
//printf("%d %f\n", tmp_s, (float)tmp_s / 32768);
    if (!(size2 % 2))
      outs[0][i] = *out = bq_process(&app->filter.biquad, (float)tmp_s / 32768);
    else
    {
      outs[1][i] = *out = bq_process(&app->filter.biquad, (float)tmp_s / 32768);
      ++i;
    }
    out++; 
    data_ptr += app->header.byte_per_block / app->header.nb_channels;
  }
  fft(app->fft_buffer, outs[0], 512);
  return (paContinue);
}

void    decode_audio(t_app *app)
{
  PaStream  *stream;

  Pa_OpenDefaultStream(&stream, 0, app->header.nb_channels,
      paFloat32, app->header.sample_rate,
      BUF_SIZE, audio_callback, app);
  Pa_StartStream(stream);
}
